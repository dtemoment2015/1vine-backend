<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CatalogListTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_catalog()
    {
        
        $this->withoutMiddleware();
        $response = $this->get('api/v1/dealer/catalog');
        $response->assertStatus(200);
    }
}
