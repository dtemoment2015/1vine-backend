<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNameTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('name_translations', function (Blueprint $table) {
            $table->bigInteger('name_id')->index()->unsigned();
            $table->string('title')->nullable();
            $table->string('locale', 16)->index();
            $table->unique(['name_id', 'locale']);
            $table->foreign('name_id')->references('id')->on('names')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('name_translations');
    }
}
