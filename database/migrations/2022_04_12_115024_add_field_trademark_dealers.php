<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldTrademarkDealers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     //
    public function up()
    {
        Schema::table('dealers', function (Blueprint $table) {
            $table->string('trademark')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dealers', function (Blueprint $table) {
            $table->dropColumn('trademark');
        });
    }
}
