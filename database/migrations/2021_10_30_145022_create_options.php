<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('options', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->unique()->nullable()->index();
            $table->string('sku', 32)->nullable()->index();
            $table->string('name')->nullable();
            $table->bigInteger('product_id')->index()->unsigned()->nullable();
            $table->bigInteger('color_id')->nullable()->index();
            $table->boolean('is_active')->default(0);
            $table->float('price', 16)->default(0);
            $table->float('price_old', 16)->default(0);
            $table->float('price_retail', 16)->default(0);
            $table->float('quantity', 16)->default(0);
            $table->float('reward', 16)->default(0);
            $table->bigInteger('position')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('options');
    }
}
