<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlockNavigation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('block_navigation', function (Blueprint $table) {
            $table->bigInteger('block_id')->index()->unsigned();
            $table->bigInteger('navigation_id')->index()->unsigned();
            $table->unique(['block_id', 'navigation_id']);
            $table->foreign('block_id')->references('id')->on('blocks')->onDelete('cascade');
            $table->foreign('navigation_id')->references('id')->on('navigations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('block_navigation');
    }
}
