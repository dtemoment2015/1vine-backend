<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldDealers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dealers', function (Blueprint $table) {
            $table->string('inn')->nullable();
            $table->string('llc_name')->nullable();
            $table->string('accountant_full_name')->nullable();
            $table->string('accountant_phone')->nullable();
            $table->string('organizationa_address')->nullable();
            $table->string('checking_account')->nullable();
            $table->string('mfo')->nullable();
            $table->string('oked')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dealers', function (Blueprint $table) {
            $table->dropColumn('inn');
            $table->dropColumn('llc_name');
            $table->dropColumn('accountant_full_name');
            $table->dropColumn('accountant_phone');
            $table->dropColumn('organizationa_address');
            $table->dropColumn('checking_account');
            $table->dropColumn('mfo');
            $table->dropColumn('oked');
        });
    }
}
