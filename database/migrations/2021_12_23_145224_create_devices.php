<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->id();
            $table->string('device_id')->index();
            $table->bigInteger('dealer_id')->index()->unsigned()->nullable();
            $table->text('token')->nullable();
            $table->string('os', 16)->default('web');
            $table->string('locale', 4)->default('ru');
            $table->unique(['device_id', 'os']);
            $table->timestamps();
            $table->foreign('dealer_id')->references('id')->on('dealers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
