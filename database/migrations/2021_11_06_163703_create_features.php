<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeatures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->bigInteger('parent_id')->index()->default(0);
            $table->bigInteger('name_id')->index()->unsigned();
            $table->boolean('is_active')->default(0);
            $table->boolean('is_filter')->default(0);
            $table->boolean('is_main')->default(0);
            $table->bigInteger('position')->default(0);
            $table->foreign('name_id')->references('id')->on('names')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('features');
    }
}
