<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealerRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dealer_role', function (Blueprint $table) {
            $table->bigInteger('dealer_id')->index()->unsigned();
            $table->bigInteger('role_id')->index()->unsigned();
            $table->bigInteger('business_id')->index()->unsigned()->nullable();
            $table->unique(['dealer_id', 'role_id', 'business_id']);
            $table->foreign('dealer_id')->references('id')->on('dealers')->onDelete('cascade');
            $table->foreign('business_id')->references('id')->on('businesses')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dealer_role');
    }
}
