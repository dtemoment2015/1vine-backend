<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->float('value', 16)->default(0);
            $table->morphs('activator'); //dealer | client
            $table->morphs('item'); //serial | status 
            $table->nullableMorphs('extra'); //raffle | order
            $table->string('barcode'); //serial code
            $table->string('secret');
            $table->boolean('is_cancel')->default(0);
            $table->boolean('is_confirm')->default(0);
            $table->bigInteger('user_id')->index()->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
