<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderPurchases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_purchases', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('order_id')->index()->unsigned();
            $table->bigInteger('option_id')->index()->nullable();
            $table->string('display_name')->nullable();
            $table->string('sku')->nullable();
            $table->string('image')->nullable();
            $table->float('price', 16)->default(0);
            $table->float('price_old', 16)->default(0);
            $table->integer('quantity')->default(1);
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_purchases');
    }
}
