<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSerials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('serials', function (Blueprint $table) {
            $table->id();
            $table->string('barcode', 32)->nullable()->index();
            $table->string('name')->nullable();
            $table->bigInteger('option_id')->index()->unsigned();
            $table->boolean('is_active')->default(0);
            $table->boolean('is_paid')->default(0);
            $table->boolean('is_wrote')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('option_id')->references('id')->on('options')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('serials');
    }
}
