<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRubricTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rubric_translations', function (Blueprint $table) {
            $table->bigInteger('rubric_id')->index()->unsigned();
            $table->string('title')->nullable();
            $table->string('locale', 16)->index();
            $table->unique(['rubric_id', 'locale']);
            $table->foreign('rubric_id')->references('id')->on('rubrics')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rubric_translations');
    }
}
