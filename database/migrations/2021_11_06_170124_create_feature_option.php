<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeatureOption extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('option_value', function (Blueprint $table) {
        
            $table->bigInteger('feature_id')->index()->unsigned();
            $table->bigInteger('option_id')->index()->unsigned();
            $table->bigInteger('value_id')->index()->unsigned();
            $table->unique(['feature_id', 'option_id', 'value_id']);
            $table->foreign('option_id')->references('id')->on('options')->onDelete('cascade');
            $table->foreign('feature_id')->references('id')->on('features')->onDelete('cascade');
            $table->foreign('value_id')->references('id')->on('values')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('option_value');
    }
}
