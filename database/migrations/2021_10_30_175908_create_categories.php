<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('parent_id')->index()->default(0);
            $table->boolean('is_active')->default(0);
            $table->boolean('in_catalog')->default(1);
            $table->string('code')->unique()->nullable()->index();
            $table->string('image')->nullable();
            $table->string('background')->nullable();
            $table->string('color')->nullable();
            $table->tinyInteger('level')->default(0);
            $table->bigInteger('position')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
