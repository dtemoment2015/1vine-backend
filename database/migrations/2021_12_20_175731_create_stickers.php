<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStickers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stickers', function (Blueprint $table) {
            $table->id();
            $table->string('barcode')->unique()->nullable();
            $table->string('secret_dealer')->unique()->nullable();
            $table->string('secret_buyer')->unique()->nullable();
            $table->boolean('is_active')->default(true);
            $table->bigInteger('serial_id')->index()->unsigned()->nullable();
            $table->string('group')->nullable();
            $table->timestamps();
            // $table->foreign('serial_id')->references('id')->on('serials')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stickers');
    }
}
