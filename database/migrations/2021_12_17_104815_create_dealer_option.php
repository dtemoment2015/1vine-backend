<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealerOption extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dealer_option', function (Blueprint $table) {
            $table->bigInteger('dealer_id')->index()->unsigned();
            $table->bigInteger('option_id')->index()->unsigned();
            $table->bigInteger('count')->default(1);
            $table->unique(['dealer_id', 'option_id']);
            $table->foreign('dealer_id')->references('id')->on('dealers')->onDelete('cascade');
            $table->foreign('option_id')->references('id')->on('options')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dealer_option');
    }
}
