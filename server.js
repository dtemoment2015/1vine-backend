
var io = require('socket.io')(6001, {
    cors: {
        origin: '*',
    }
}),
    Redis = require('ioredis'),
    redis = new Redis();

redis.psubscribe('*', function (err, count) {
});

// io.set('origins', '*:*');

redis.on('pmessage', function (subscribed, channel, message) {
    if (message) {
        message = JSON.parse(message);
        if (message.data && message.event) {
            io.emit(message.event, message.data);
        }
    }
});

io.on('connection', function (socket) {
    socket.emit('status', { data: 'connected' });
});