<?php

return [

    "system" => "Tizimda xatolik yuz berdi. Iltimos keyinroq qayta urinib ko'ring",

    "form_validate" => "Ma'lumotlar noto'g'ri to'ldirilgan",

    "access" => "Kirish rad etildi",

    "phone_format" => "Telefon raqamining formati noto'g'ri",

    "password_format" => "Parol talab qilinadigan parametr (kamida 5 belgi)",

    "not_found" => "Element ma'lumotlar bazasida topilmadi",

    "code_format" => "Kod talab qilinadigan parametr",

    "code_not_found" => "Tasdiqlash kodi yaroqsiz",

    "password_confirmed" => "Parollar mos kelmaydi",

    "shop_format" => "Do'kon ma'lumotlarini kiriting",

    "password_confirmed" => "Parollar mos kelmaydi",

    "last_name_format" => "Iltimos, familiyangizni kiriting (kamida 1 belgi)",

    "first_name_format" => "Iltimos, ism kiriting (kamida 1 belgi)",

    "hash_not_found" => "Tasdiqlash kaliti yaroqsiz",

    "email_exist" => "Bu email allaqachon mavjud",

    "phone_exist" => "Bu raqam allaqachon ma'lumotlar bazasida mavjud.",

    "email_format" => "Yaroqsiz elektron pochta formati",

    "phone_not_found" => "Foydalanuvchi topilmadi",

    "phone_code_exist" => "Kodni 2 daqiqadan so'ng yana yuborishingiz mumkin",

    "code_error" => "faollashtirish kodi noto'g'ri",


    "shop_error" => "Manzil tizimda topilmadi",

    "code_already_activated" => "Siz allaqachon kodni faollashtirgansiz",

    "no_product" => "Mahsulot mavjud emas yoki zaxirada yo‘q",

    "empty_cart" => "Sizning savatingiz bo'sh!",

    "dont_know" => "Xizmat vaqtincha ishlamayapti"
];
