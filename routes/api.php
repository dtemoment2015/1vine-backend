<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'App\Http\Controllers', 'middleware' => 'logger'], function () {
    // Route::get('test','TestController@index');
    Route::group(['prefix' => 'v1', 'namespace' => 'v1', 'middleware' => 'localization'], function () {

        Route::group(
            ['namespace' => 'Stock', 'prefix' => 'stock'],
            function () {
                Route::get('test', 'IndexController@test');
                Route::apiResource('products', 'IndexController');
                Route::apiResource('filters', 'FilterController');
            }
        );
        Route::group(['prefix' => 'dealer', 'namespace' => 'Dealer'], function () {
            Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
                Route::post('login', 'IndexController@login');
                Route::post('restore', 'IndexController@restore');
                Route::post('register', 'IndexController@register');
                Route::post('activate', 'IndexController@activate');
                Route::group(['prefix' => 'code', 'namespace' => 'Code'], function () {
                    Route::post('/', 'IndexController@index');
                    Route::post('verify', 'IndexController@verify');
                });
            });
            Route::group(
                ['middleware' => 'auth:dealer'],
                function () {
                    Route::group(['prefix' => 'profile', 'namespace' => 'Profile'], function () {
                        Route::get('/', 'IndexController@index');
                        Route::put('update', 'IndexController@update');
                        Route::post('activate', 'IndexController@activate');
                    });
                    Route::group(
                        ['middleware' => 'dealer'], // тут если не активный не будет доступа никакого!
                        function () {
                            //Главная
                            Route::group(
                                ['prefix' => 'home', 'namespace' => 'Home'],
                                function () {
                                    Route::get('/', 'IndexController@index');
                                }
                            );
                            //Каталог
                            Route::group(
                                ['namespace' => 'Catalog'],
                                function () {
                                    Route::apiResource('catalog', 'IndexController');
                                }
                            );
                            Route::group(
                                ['prefix' => 'cart', 'namespace' => 'Cart'],
                                function () {
                                    Route::get('/', 'IndexController@index');
                                    Route::post('add', 'IndexController@add');
                                    Route::delete('delete/{id}', 'IndexController@delete');
                                    Route::post('checkout', 'IndexController@checkout');
                                }
                            );
                            //Заказы
                            Route::group(
                                ['namespace' => 'Order'],
                                function () {
                                    Route::apiResource('orders', 'IndexController');
                                }
                            );
                            //Торговые точки
                            Route::group(
                                ['namespace' => 'Shop'],
                                function () {
                                    Route::apiResource('shops', 'IndexController');
                                }
                            );
                            //Ноовсти
                            Route::group(
                                ['namespace' => 'Post'],
                                function () {
                                    Route::apiResource('posts', 'IndexController');
                                }
                            );
                            //Транзакции
                            Route::group(
                                ['namespace' => 'Transaction'],
                                function () {
                                    Route::apiResource('transactions', 'IndexController');
                                }
                            );
                        }
                    );
                }
            );
            Route::group(['prefix' => 'storage', 'namespace' => 'Storage'], function () {
                Route::get('data', 'IndexController@data');
                Route::get('regions', 'IndexController@regions');
                Route::get('test', 'IndexController@test');
            });
            //FOR DEVELOPER
            Route::group(['prefix' => '_dev', 'namespace' => 'Dev'], function () {
                Route::group(['prefix' => 'dealer'], function () {
                    Route::delete('remove', 'IndexController@dealerRemove');
                    Route::delete('deactive', 'IndexController@dealerDeactive');
                    Route::post('activation', 'IndexController@dealerActivation');
                });
                Route::group(['prefix' => 'sms'], function () {
                    Route::post('reset', 'IndexController@smsReset');
                });
                Route::group(['prefix' => 'logger'], function () {
                    Route::get('list', 'IndexController@loggerList');
                    Route::delete('clear', 'IndexController@loggerClear');
                });
            });
        });

        Route::group(['prefix' => 'client', 'namespace' => 'Client'], function () {
            Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
                Route::group(['prefix' => 'code', 'namespace' => 'Code'], function () {
                    Route::post('/', 'IndexController@index');
                    Route::post('verify', 'IndexController@verify');
                });
            });

            Route::group(
                ['middleware' => 'auth:client'],
                function () {
                    Route::group(['prefix' => 'profile', 'namespace' => 'Profile'], function () {
                        Route::get('/', 'IndexController@index');
                        Route::put('update', 'IndexController@update');
                    });
                    Route::group(
                        [], // тут если не активный не будет доступа никакого!
                        function () {
                            //Каталог
                            Route::group(
                                ['namespace' => 'Catalog'],
                                function () {
                                    Route::apiResource('catalog', 'IndexController');
                                }
                            );

                            //Ноовсти
                            Route::group(
                                ['namespace' => 'Post'],
                                function () {
                                    Route::apiResource('posts', 'IndexController');
                                }
                            );
                            //Транзакции
                            Route::group(
                                ['namespace' => 'Transaction'],
                                function () {
                                    Route::apiResource('transactions', 'IndexController');
                                }
                            );
                        }
                    );
                }
            );
            Route::group(['prefix' => 'storage', 'namespace' => 'Storage'], function () {
                Route::get('data', 'IndexController@data');
                Route::get('regions', 'IndexController@regions');
            });
        });


        Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
            Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
                Route::post('login', 'IndexController@login');
            });

            Route::apiResources(['storages' => 'StorageController']);
            Route::group(
                ['middleware' => 'auth:admin'],
                function () {
                    Route::group(['prefix' => 'profile', 'namespace' => 'Profile'], function () {
                        Route::get('/', 'IndexController@index');
                    });
                    Route::group(['prefix' => 'warehouse', 'namespace' => 'Warehouse'], function () {
                        Route::apiResources([
                            'brands' => 'BrandController',
                            'names' => 'NameController',
                            'names.features' => 'FeatureController',
                            'products' => 'ProductController',
                            'features' => 'FeatureController',
                            'features.subfeatures' => 'SubFeatureController',
                            'values' => 'ValueController',

                            'categories' => 'CategoryController',
                            'categories.subcategories' => 'CategoryController',
                            'options' => 'OptionController',
                            'options.serials' => 'OptionSerialController',
                            'serials' => 'SerialController',
                            'options.images' => 'ImageController',
                            'stickers' => 'StickerController',
                            'groups' => 'GroupController',
                        ]);
                    });
                    Route::apiResources([
                        'dealers' => 'DealerController',
                        'dealers.orders' => 'DealerOrderController',
                        'dealers.shops' => 'DealerShopController',
                        'dealers.transactions' => 'DealerTransactionController',

                        'clients' => 'ClientController',
                        'clients.transactions' => 'ClientTransactionController',

                        'stats' => 'StatController',
                        'points' => 'PointController',
                        'media' => 'MediaController',
                        'ratings' => 'RatingController',
                        'roles' => 'RoleController',
                        'blocks' => 'BlockController',
                        'users.orders' => 'UserOrderController',
                        'users.shops' => 'UserShopController',
                        'users.points' => 'UserPointController',
                        'statuses' => 'StatusController',
                        'shops' => 'ShopController',
                        'shops.users' => 'ShopUserController',
                        'navigations' => 'NavigationController',
                        'texts' => 'TextController',
                        'imports' => 'ImportController',
                        'users' => 'UserController',
                        'orders' => 'OrderController',
                        'transactions' => 'TransactionController',
                        'posts' => 'PostController',

                        'orders.purchases' => 'OrderPurchaseController',
                        'orders.statuses' => 'OrderStatusController',
                        'orders.payments' => 'OrderPaymentController',

                        'purchases' => 'OrderPurchaseController',
                        //
                        'my-shops' => 'MyShopController',
                        'my-dealers' => 'MyDealerController',
                        'my-orders' => 'MyOrderController',
                        //
                        //'supervisor-shops' => 'SupervisorShopController',
                        'supervisor-orders' => 'SupervisorOrderController',
                        'supervisor-users' => 'SupervisorUserController',
                    ]);
                }
            );
        });
        Route::group(['prefix' => 'exchange', 'namespace' => 'Exchange', 'middleware' => 'basic'], function () {
            Route::post('1c', 'OneCController@store');
        });
    });
});
