<?php

namespace App\Observers\v1;

use App\Models\Text;
use Illuminate\Support\Facades\Cache;

class TextObserver
{
    /**
     * Handle the Text "created" event.
     *
     * @param  \App\Models\Text  $text
     * @return void
     */
    public function created(Text $text)
    {
        foreach (config('translatable.locales') as $lang) {
            Cache::forget('text-' . $lang);
        }
    }

    /**
     * Handle the Text "updated" event.
     *
     * @param  \App\Models\Text  $text
     * @return void
     */
    public function updated(Text $text)
    {
        foreach (config('translatable.locales') as $lang) {
            Cache::forget('text-' . $lang);
        }
    }

    /**
     * Handle the Text "deleted" event.
     *
     * @param  \App\Models\Text  $text
     * @return void
     */
    public function deleted(Text $text)
    {
        foreach (config('translatable.locales') as $lang) {
            Cache::forget('text-' . $lang);
        }
    }

    /**
     * Handle the Text "restored" event.
     *
     * @param  \App\Models\Text  $text
     * @return void
     */
    public function restored(Text $text)
    {
        foreach (config('translatable.locales') as $lang) {
            Cache::forget('text-' . $lang);
        }
    }

    /**
     * Handle the Text "force deleted" event.
     *
     * @param  \App\Models\Text  $text
     * @return void
     */
    public function forceDeleted(Text $text)
    {
        //
    }
}
