<?php

namespace App\Providers;

use App\Models\Text;
use App\Observers\v1\TextObserver;
use App\Services\SmsService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('smsProvider', SmsService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Text::observe(TextObserver::class);
    }
}
