<?php

namespace App\Providers;

use App\Repositories\Interfaces\StockRepositoryInterface;
use App\Repositories\StockRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            StockRepositoryInterface::class,
            StockRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
