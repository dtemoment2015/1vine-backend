<?php

namespace App\Jobs;

use App\Models\Logger;
use Illuminate\Bus\Queueable;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class LoggerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $uri,
        $method,
        $request_body,
        $response,
        $header,
        $status;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        $uri,
        $method,
        $request_body,
        $response,
        $header,
        $status
    ) {
        $this->uri = $uri;
        $this->method = $method;
        $this->request_body = $request_body;
        $this->response = $response;
        $this->header = $header;
        $this->status = $status;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Logger::create([
            'uri' => $this->uri,
            'method' => $this->method,
            'request_body' => $this->request_body,
            'response' =>  $this->response,
            'header' => $this->header,
            'status' => $this->status
        ]);
    }
}
