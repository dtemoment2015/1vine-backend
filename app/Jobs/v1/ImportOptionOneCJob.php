<?php

namespace App\Jobs\v1;

use App\Models\Warehouse\Option;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportOptionOneCJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public  $option;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($option)
    {
        $this->option = $option;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $price = (int) optional($this->option)->Price;


        $price  = round($price / 100) * 100;

        $quantity = (int) optional($this->option)->Amount;

        if (
            ($name = optional($this->option)->Name) &&
            ($sku = optional($this->option)->Artikul)
        ) {

            $option = Option::firstOrCreate(
                ['sku' => $sku],
                [
                    'sku' => $sku,
                    'name' => $name,
                    'slug' => $sku . time() . '' . rand(1111, 9999)
                ],
            );

            $option->price = (int) $price;
            $option->quantity = (int) $quantity;

            $option->save();
        }
    }
}
