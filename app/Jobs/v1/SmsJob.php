<?php

namespace App\Jobs\v1;

use App\Facades\Sms;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SmsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user, $message;

    public function __construct($user = null, $message = '')
    {
        $this->user = $user;
        $this->message = $message;
    }

    public function handle()
    {
        Sms::send(
            $this->user,
            $this->message
        );
    }
}
