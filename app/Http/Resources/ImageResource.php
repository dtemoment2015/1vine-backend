<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ImageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'file' => url('storage/options/' . $this->file),
            'file_large' => url('storage/options/x600/' . $this->file),
            'file_medium' => url('storage/options/x200/' . $this->file),
            'position' => $this->position
        ];
    }
}
