<?php

namespace App\Http\Resources\v1\Dealer;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BlockCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->mapWithKeys(function ($item) {
            return [$item->code => NavigationResource::collection($item->navigations)];
        });
    }
}
