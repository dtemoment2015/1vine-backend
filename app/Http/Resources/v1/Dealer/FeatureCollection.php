<?php

namespace App\Http\Resources\v1\Dealer;

use Illuminate\Http\Resources\Json\ResourceCollection;

class FeatureCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return  $this->collection
            ->mapToGroups(function ($item) {
                return [
                    $item->key->feature->parent->title => [
                        'id' => $item->key->feature->id,
                        'root_id' => $item->key->feature->parent->id,
                        'title' => $item->key->feature->title,
                        'values' => ['id' => $item->id, 'title' => $item->title],
                    ]
                ];
            })
            ->map(function ($item, $key) {
                return  [
                    'id' => $item->pluck('root_id')->first(),
                    'title' => $key,
                    'features' => $item
                ];
            })
            ->map(function ($item) {
                $item['features'] = $item['features']->mapToGroups(function ($item) {
                    return [
                        $item['title'] => [
                            'values' => $item['values'],
                            'id' => $item['id']
                        ]
                    ];
                })->map(function ($item, $key) {
                    return  [
                        'id' => $item->pluck('id')->first(),
                        'title' => $key,
                        'values' => $item->pluck('values')
                    ];
                })
                    ->values();
                return $item;
            })

            ->values();
    }
}
