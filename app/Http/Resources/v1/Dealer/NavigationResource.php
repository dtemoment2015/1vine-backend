<?php

namespace App\Http\Resources\v1\Dealer;

use Illuminate\Http\Resources\Json\JsonResource;

class NavigationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $badge = 0;

        switch ($this->code) {
            case 'cart': {
                    $badge = optional($request)->_user->cart()->limit(1)->count();
                    break;
                }
        }

        return [
            'id' => $this->id,
            'code' =>  (string) $this->code,
            'title' => (string) $this->title,
            'label' => (string) $this->title,
            'icon' =>  (string) $this->icon,
            'activeIcon' =>  (string) $this->activeIcon,
            'inActiveIcon' =>   (string) $this->activeIcon ?  $this->activeIcon.'-outline'  : "",
            'type' => (string) $this->type,
            'value' => (string) $this->value,
            'route' => (string) $this->value,
            'component' => (string) $this->component,
            'color' => (string) $this->color,
            'alphaClr' => (string) $this->alphaClr,

            'badge' => $badge,
            'sections' =>  SectionResource::collection($this->whenLoaded('sections')),
        ];
    }
}
