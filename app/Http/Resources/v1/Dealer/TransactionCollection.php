<?php

namespace App\Http\Resources\v1\Dealer;

use App\Models\Storage\Navigation;
use Illuminate\Http\Resources\Json\ResourceCollection;

class TransactionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $page =  new NavigationResource(
            Navigation::whereCode('transactions')
                ->limit(1)
                ->firstOrFail()
        );

        return  [
            'id' => $page->id,
            'code' => $page->code,
            'title' => $page->title,
            'icon' => $page->icon,
            'type' => $page->type,
            'value' => $page->value,
            'badge' => 0,
            'data' => TransactionResouce::collection($this->collection)
        ];
    }
}
