<?php

namespace App\Http\Resources\v1\Dealer;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'image' => $this->image,
            'annotation' => $this->annotation,
            'created_at' => $this->created_at,
            'code' => $this->code,
            'contents' =>   ContentResource::collection($this->whenLoaded('contents')),
        ];
    }
}
