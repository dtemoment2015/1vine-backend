<?php

namespace App\Http\Resources\v1\Dealer;

use Illuminate\Http\Resources\Json\JsonResource;

class ContentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => (int) $this->id,
            'type' => (string) $this->type,
            'value' => (string) $this->value,
            'position' => (int) $this->position,
        ];
    }
}
