<?php

namespace App\Http\Resources\v1\Dealer;

use Illuminate\Http\Resources\Json\ResourceCollection;

class FilterCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return $this->collection;
        return $this->collection

        // ->groupBy(['feature_id', function ($item) {
        //     return $item['value'];
        // }], $preserveKeys = true)

            ->mapToGroups(function ($item) {
                return [
                    $item->feature_id => [
                        'title' => $item->feature->title,
                        'values' => $item->value,
                        'feature_id' => $item->feature_id,
                        'value_id' => $item->value_id,
                    ]
                ];
            })
            ->map(function ($item, $key) {
                return  [
                    'id' => $key,
                    'title' => $item->pluck('title')->first(),
                    'data' => $item
                        ->unique('value_id')
                        ->map(function($item){
                        return [
                            'id' => $item['value_id'],
                            'feature_id' => $item['feature_id'],
                            'title' => $item['values']['title'],
                        ];
                    })->values()
                ];
            })
            ;
    }
}
