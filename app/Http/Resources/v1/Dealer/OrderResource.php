<?php

namespace App\Http\Resources\v1\Dealer;

use App\Http\Resources\v1\StatusResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            // 'price' => $this->purchases()->limit(1)->sum(DB::raw('price * quantity')),

            'price' => (float)  $this->when($this->price, function () {
                return $this->price;
            }, function () {
                return $this->purchases()->limit(1)->sum(DB::raw('price * quantity'));
            }),
            'user' =>  $this->whenLoaded('user'),
            'is_paid' => (bool) $this->is_paid,
            'paid_at' => (string) $this->paid_at,
            'created_at' => (string) $this->created_at,
            'customer' => $this->customer,
            'purchases' => PurchaseResource::collection($this->whenLoaded('purchases')),
            'status' => $this->when($this->status, function () {
                return new StatusResource($this->status->status);
            }),
            'status_list' => $this->when(optional($this)->status_list, function () {
                return StatusResource::collection($this->status_list);
            }),

            'shop' =>  optional($this->customer)->shop,
        ];
    }
}
