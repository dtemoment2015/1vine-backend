<?php

namespace App\Http\Resources\v1\Dealer;

use App\Http\Helpers\ResourceMap;
use Illuminate\Http\Resources\Json\JsonResource;

class SectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => (string)  $this->code,
            'typeof' => (string)  $this->name,
            'title' => (string) $this->title,
            'type' => (string) $this->type,
            'data' => ResourceMap::_get($request, $this->type)
        ];
    }
}
