<?php

namespace App\Http\Resources\v1\Dealer;

use App\Http\Helpers\ResourceMap;
use App\Http\Resources\v1\Admin\RoleAdminResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class DealerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        if (!optional($request)->_user) {
            $request->request->add(
                [
                    '_user' => $this,
                ]
            );
        }

        $transactions = $this->transactions();

        return [
            'id' => $this->id,
            'is_active' => (bool) $this->is_active,
            'full_name' => (string) $this->full_name,
            'first_name' => (string)  $this->first_name,
            'last_name' => (string)  $this->last_name,
            'phone' => (string) $this->phone,
            'email' => (string)  $this->email,
            'channel' => (string)  $this->channel,

            'code' => $this->code,
            'inn' => $this->inn,
            'llc_name' => $this->llc_name,
            'accountant_full_name' => $this->accountant_full_name,
            'accountant_phone' => $this->accountant_phone,
            'organizationa_address' => $this->organizationa_address,
            'checking_account' => $this->checking_account,
            'mfo' => $this->mfo,
            'oked' => $this->oked,
            'trademark' => $this->trademark,

            //оптимизировать
            'bonus' => (float) $transactions->sum('value'),
            'bonus_withdraw' => (float) $transactions->where('value', '<', 0)->sum('value'),
            'bonus_withdraw_month' => (float) $transactions->whereYear('created_at', Carbon::now()->year)
                ->whereMonth('created_at', Carbon::now()->month)->sum('value'),

            'shops' => ShopResource::collection($this->whenLoaded('shops')),
            'role' => new  RoleAdminResource($this->whenLoaded('role')),
            'cart' => new CartCollection($this->cart),
            'bottom_bar' => ResourceMap::_get($request, 'bottom_bar'),

        ];
    }
}
