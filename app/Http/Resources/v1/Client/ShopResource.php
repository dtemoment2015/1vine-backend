<?php

namespace App\Http\Resources\v1\Client;

use App\Http\Resources\v1\LocationResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ShopResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => (string)  $this->name,
            'name' => (string)  $this->name,
            'code' => $this->code,
            'lat' => (float) $this->lat,
            'lon' => (float) $this->lon,
            'address' => (string) $this->address,
            'description' =>(string) $this->description,
            'location_id' => (int) $this->location_id,
            'Client_id' => (int) $this->Client_id,
            'location' => new LocationResource( $this->location),
            'address_full' => (string)  optional(optional($this->location)->parent)->title . ' ' . optional($this->location)->title  . ' ' .  $this->address,
            'business' => new BusinessResource($this->whenLoaded('business')),
            'Client' => new ClientResource($this->whenLoaded('Client')),
            'media' =>  MediaResource::collection( $this->whenLoaded('media') )
        ];
    }
}
