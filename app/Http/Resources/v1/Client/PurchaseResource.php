<?php

namespace App\Http\Resources\v1\Client;

use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'option_id' => $this->option_id,
            'display_name' =>(string) $this->display_name,
            'sku' => (string) $this->sku,
            'image' => (string) $this->image,
            'price' => (int) $this->price,
            'price_old' => (int) $this->price_old,
            'quantity' => (int) $this->quantity,
            'price_total' => (int) $this->price_total,

        ];
    }
}
