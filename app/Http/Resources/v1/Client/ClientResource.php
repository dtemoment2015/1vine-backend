<?php

namespace App\Http\Resources\v1\Client;

use App\Http\Helpers\ResourceMap;
use App\Http\Resources\v1\Admin\RoleAdminResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        if (!optional($request)->_user) {
            $request->request->add(
                [
                    '_user' => $this,
                ]
            );
        }

        $transactions = $this->transactions();

        return [
            'id' => $this->id,
            'is_active' => (bool) $this->is_active,
            'full_name' => (string) $this->full_name,
            'first_name' => (string)  $this->first_name,
            'last_name' => (string)  $this->last_name,
            'phone' => (string) $this->phone,
            'email' => (string)  $this->email,
            'birthday' => (string)  $this->birthday,
            'channel' => (string)  $this->channel,
            'bonus' => (float) $transactions->sum('value'),

        ];
    }
}
