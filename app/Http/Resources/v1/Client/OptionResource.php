<?php

namespace App\Http\Resources\v1\Client;

use App\Http\Resources\ImageResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class OptionResource extends JsonResource
{


    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $images = ImageResource::collection($this->whenLoaded('images'));

        return  [
            'id' => $this->id,
            'slug' => $this->slug,
            'display_name' => optionName($this),
            'image' => $this->when(count($images), function () use ($images) {
                return url('storage/options/x200/' . $images[0]->file);
            }),
            'sku' => $this->sku,
            'price' => $this->price,
            'price_retail' => $this->price_retail,
            'price_old' => $this->price_old,
            'discount_percent' => (int) round(($this->price_old > 0 && $this->price_old > $this->price) ? (100 - ($this->price * 100 / $this->price_old)) : 0),
            'reward' => $this->reward,
            'quantity' => $this->quantity,
            'images' => $images,
            'contents' =>   ContentResource::collection($this->whenLoaded('contents')),
            'characteristics' =>  new FeatureCollection($this->whenLoaded('values')),
        ];
    }
}
