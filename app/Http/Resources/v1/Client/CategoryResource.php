<?php

namespace App\Http\Resources\v1\Client;

use App\Models\Warehouse\Brand;
use App\Models\Warehouse\OptionValue;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => (string)  $this->code,
            'title' => (string) $this->title,
            'childs' => self::collection($this->whenLoaded('childs')),
            'filter' => $this->when(($this->count != null || $this->count == '0'), function () {
              
                $product = $this->products()->join('options', function ($join) {
                    $join->on('products.id', '=', 'options.product_id')
                        // clone logic stock
                        ->where('options.is_active', true);
                });
                
                // $product = $this->options()->where('is_active', true);

                return  [
                  
                    'price' => [
                        'min' => (float) $product->min('options.price'),
                        'max' => (float) $product->max('options.price')
                    ],

                    // 'price' => [
                    //     'min' => (float) $product->min('price'),
                    //     'max' => (float) $product->max('price')
                    // ],

                    'brands' => BrandResource::collection($product
                        ->groupBy('brand_id')
                        ->get()
                        ->pluck('brand')),

                    // 'brands' => BrandResource::collection(
                    //     Brand::whereHas('products.options.categories', function($query){
                    //         $query->whereId($this->id);
                    //     })->groupBy('id')->get()
                    // ),
                        
                    'features' => new FilterCollection(OptionValue::whereHas(
                        'option.product.categories',
                        function ($query) {
                            $query->whereId($this->id);
                        }
                    )->with(
                        'feature',
                        'value'
                    )
                        ->get())
                ];
            })
        ];
    }
}
