<?php

namespace App\Http\Resources\v1\Client;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResouce extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        $item = $this->when(
            $this->item_type,
            function () {
                if ($this->item_type == 'serial') {

                    return [
                        'title' => optionName($this->item->option),
                        'type' => 'sale',
                    ];

                } elseif ($this->item_type == 'status') {

                    $item = $this->item;

                    return [
                        'title' =>  $item->title,
                        'type' => $item->code,
                    ];
                    
                }
            },
        );

        return [
            'id' => $this->id,
            'barcode' => (string) $this->barcode,
            'type' => (string) optional($item)['type'],
            'title' => (string) optional($item)['title'],
            'value' => $this->value,
            'created_at' => $this->created_at,
        ];
    }
}
