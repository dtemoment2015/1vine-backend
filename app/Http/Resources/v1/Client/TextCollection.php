<?php

namespace App\Http\Resources\v1\Client;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TextCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->mapToGroups(function ($item) {
                return [$item->group => [$item->key => $item->title]];
            })
            ->map(function ($i) {
                return $i->collapse();
            })
            ->toArray();
    }
}
