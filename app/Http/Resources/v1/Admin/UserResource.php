<?php

namespace App\Http\Resources\v1\Admin;

use App\Models\Storage\Navigation;
use Illuminate\Http\Resources\Json\JsonResource;


class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'phone' => $this->phone,
            'code' => $this->code,
            'name' => $this->name,
            'routes' => optional(optional($this->role)->navigations)->where('platform', $request->header('platform'))->load('blocks')->values(),
        ];
    }
}
