<?php

namespace App\Http\Resources\v1\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'vendor_code' => $this->vendor_code,
            'display_name' => optional($this->brand)->name . ' ' . optional($this->name)->title . ' ' . $this->title,
            'name' => $this->name,
            'brand' => $this->brand,
            'position' => $this->position,
            'categories' => $this->categories,
            'options' =>  OptionAdminResource::collection( $this->whenLoaded('options')),
            'title' => $this->title,
            'translations' => $this->translations,
        ];
    }
}
