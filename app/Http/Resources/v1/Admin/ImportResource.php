<?php

namespace App\Http\Resources\v1\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class ImportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => (string) $this->type,
            'platform' =>  (string)  $this->platform,
            'model' =>  (string)  $this->model,
            'exchange_type' =>  (string)  $this->exchange_type,
            'created_at' =>   (string)  $this->created_at,
            'file' => optional(optional($this->getMedia('import_file'))->last())->original_url,
        ];
    }
}
