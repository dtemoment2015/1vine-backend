<?php

namespace App\Http\Resources\v1\Admin;

use App\Http\Resources\v1\ImageBrandResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BrandAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => (string) $this->name,
            'code' => (string) $this->code,
            'is_active' => (bool) $this->is_active,
            'image' => (string) $this->image ?  url('storage/brands/x220/' . $this->image) : "",
            'position' => (int) $this->position,
        ];
    }
}
