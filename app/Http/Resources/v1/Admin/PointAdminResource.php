<?php

namespace App\Http\Resources\v1\Admin;

use App\Http\Resources\v1\Dealer\MediaResource;
use App\Http\Resources\v1\Dealer\ShopResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PointAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'shop' => new ShopResource( $this->whenLoaded('shop')),
            'user' => $this->whenLoaded('user'),
            'media' => MediaResource::collection( $this->whenLoaded('media')),
            'device' => $this->device,
            'lat' => (float) $this->lat,
            'lon' => (float) $this->lon,
            'created_at' => $this->created_at,
        ];
    }
}
