<?php

namespace App\Http\Resources\v1\Admin;

use App\Http\Resources\v1\Dealer\MediaResource;
use Illuminate\Http\Resources\Json\JsonResource;


class ShopAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'address' => $this->address,
            'title' => $this->title,
            'code' => $this->code,
            'name' => $this->name,
            'description' => $this->description,
            'location_id' =>  $this->location_id,
            'location' => $this->whenLoaded('location'),
            'business_id' => $this->business_id,
            'business' => $this->whenLoaded('business'),
            'lat' => (float) $this->lat,
            'lon' => (float) $this->lon,
            'dealer_id' => $this->dealer_id,
            'dealer' => new DealerAdminResource( $this->whenLoaded('dealer') ),
            'media' =>  MediaResource::collection( $this->whenLoaded('media') ),
            'created_at' => $this->created_at,
            'translations' => $this->translations,
        ];
    }
}
