<?php

namespace App\Http\Resources\v1\Admin;

use App\Http\Resources\v1\Dealer\OptionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SerialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'barcode' => $this->barcode,
            'is_active' => $this->is_active,
            'option' => new OptionResource( $this->whenLoaded('option')),
            'is_paid' => $this->is_paid,
            'is_wrote' => $this->is_wrote,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
