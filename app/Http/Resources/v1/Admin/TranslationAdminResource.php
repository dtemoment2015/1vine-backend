<?php

namespace App\Http\Resources\v1\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class TranslationAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $item = $this->when(
            $this->item_type,

            function () {
                if ($this->item_type == 'serial') {
                    return [
                        'title' => optionName($this->item->option, true),
                        'type' => 'sale',
                    ];

                } elseif ($this->item_type == 'status') {
                    $item = $this->item;
                    return [
                        'title' =>  optional($item)->title,
                        'type' => optional($item)->code,
                    ];
                    
                
                } elseif ($this->item_type == 'order') {
                    $item = $this->item;
                    return [
                        'title' => 'Заказ # '. optional($item)->id,
                        'type' => 'order',
                    ];
                    
                }
            },
        );

        return [
            'id' => $this->id,
            'barcode' => (string) $this->barcode,
            'is_confirm' => (bool) $this->is_confirm,
            'type' => (string) optional($item)['type'],
            'title' => (string) optional($item)['title'],
            'value' => $this->value,
            'activator_type' => $this->activator_type,
            'activator' => $this->activator,
            'created_at' => $this->created_at,
        ];
    }
}
