<?php

namespace App\Http\Resources\v1\Admin;

use App\Http\Resources\ImageResource;

use Illuminate\Http\Resources\Json\JsonResource;


class OptionAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'is_active' => $this->is_active,
            'name' => $this->name,
            'display_name' => optionName($this, true),
            'sku' => $this->sku,
            'reward' => $this->reward,
            'price' => $this->price,
            'price_retail' => $this->price_retail,
            'price_old' => $this->price_old,
            'quantity' => $this->quantity,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'values' =>  new ValueAdminCollection($this->whenLoaded('values')),
            'product' => new ProductAdminResource($this->whenLoaded('product')),
            'images' =>    ImageResource::collection($this->whenLoaded('images')),
            'modifications' =>    self::collection($this->whenLoaded('modifications')),
            // 'categories' =>    CategoryAdminResource::collection($this->whenLoaded('categories')),
            'contents' =>   ContentAdminResource::collection( $this->whenLoaded('contents')),
        ];
    }
}
