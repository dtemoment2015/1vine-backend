<?php

namespace App\Http\Resources\v1\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class ContentAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'value' => $this->value,
            'position' => $this->position,
            'translations' => $this->translations,
            'fields' => [
                [
                    'col' => 'col-12 col-md-12',
                    'key' => 'is_remove',
                    'label' => 'Удалить?',
                    'type' => 'checkbox'
                ],
                [
                    'col' => 'col-12 col-md-12',
                    'key' => 'value',
                    'label' => 'Описание',
                    'type' => 'html',
                    'isMultiLang' => true,
                ]
            ]
        ];
    }
}
