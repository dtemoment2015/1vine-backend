<?php

namespace App\Http\Resources\v1\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class RatingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'role' =>$this->role,
            'purchases_total' => (int) $this->purchases_total,
            'orders_count' => (int) $this->orders_count,
            'dealers_count' => (int) $this->dealers_count,
            'orders_done_count' => (int) $this->orders_done_count,
            'shops_count' => (int) $this->shops_count,
            'order_prices_total' => (int) $this->order_prices_total,
            'dealers_count' => $this->dealers_count,
            'created_at' => $this->created_at,
        ];
    }
}
