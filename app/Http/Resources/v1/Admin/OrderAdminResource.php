<?php

namespace App\Http\Resources\v1\Admin;

use App\Http\Resources\v1\Dealer\DealerResource;
use App\Http\Resources\v1\Dealer\PurchaseResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        //Везде добавить ресусры на объекты
        $dealer = $this->dealer;

        return [
            'id' => $this->id,
            'price' => (int) $this->price,
            'reward' => (int) $this->reward,
            'payment' => (int) $this->payment,
            'dealer' => new DealerResource($dealer),
            'role' => optional($dealer)->role,
            'purchases' =>  PurchaseResource::collection($this->whenLoaded('purchases')),
            'user' =>  $this->user,
            'customer' =>  $this->customer,
            'attach_users' =>  $this->whenLoaded('attach_users'),
            'status' =>  optional($this->status)->status,
            'shop' =>  new ShopAdminResource(optional($this->customer)->shop),
            'is_paid' =>  $this->is_paid,
            'paid_at' =>  $this->paid_at,
            'created_at' =>  $this->created_at,
        ];
    }
}
