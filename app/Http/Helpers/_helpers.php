<?php

if (!function_exists('queryArray')) {
    /**
     * @param string $query
     * @return array
     */
    function queryArray($query = ''): array
    {
        $returnArray = [];
        $dataArray = explode('-', $query);
        foreach ($dataArray as $item) {
            if (is_numeric($item)) {
                $returnArray[] = $item;
            }
        }
        return $returnArray;
    }
}

if (!function_exists('cleanUTF')) {
    /**
     * @param string $query
     * @return array
     */
    function cleanUTF($name, $id = null)
    {

        $name = str_replace(array('š', 'č', 'đ', 'č', 'ć', 'ž', 'ñ'), array('s', 'c', 'd', 'c', 'c', 'z', 'n'), $name);

        $name = str_replace(array('Š', 'Č', 'Đ', 'Č', 'Ć', 'Ž', 'Ñ'), array('S', 'C', 'D', 'C', 'C', 'Z', 'N'), $name);

        $cyr = array('а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'љ', 'м', 'н', 'њ', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'џ', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'Љ', 'М', 'Н', 'Њ', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Џ', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я');

        $lat = array('a', 'b', 'v', 'g', 'd', 'e', 'e', 'z', 'z', 'i', 'j', 'k', 'l', 'lj', 'm', 'n', 'nj', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'c', 'dz', 's', 's', 'i', 'j', 'j', 'e', 'ju', 'ja', 'A', 'B', 'V', 'G', 'D', 'E', 'E', 'Z', 'Z', 'I', 'J', 'K', 'L', 'Lj', 'M', 'N', 'Nj', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'C', 'Dz', 'S', 'S', 'I', 'J', 'J', 'E', 'Ju', 'Ja');

        $name = str_replace(
            $cyr,
            $lat,
            $name
        );

        $name =  preg_replace('/\s/u', '-', $name);
        $name =  strtolower(trim($name));
        $name = preg_replace('/[^a-z0-9-]+/', '', $name);
        $name  =   preg_replace('/-+/', '-', $name);


        if ($id) {
            $name  = $name . '-' . $id;
        }

        return $name;
    }
}


if (!function_exists('is_base64')) {
    /**
     * @param string $query
     * @return array
     */
    function is_base64($s)
    {
        // Check if there are valid base64 characters
        if (!preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $s)) return false;

        // Decode the string in strict mode and check the results
        $decoded = base64_decode($s, true);
        if (false === $decoded) return false;

        // Encode the string again
        if (base64_encode($decoded) != $s) return false;

        return true;
    }
}
if (!function_exists('getIds')) {
    /**
     * @param string $query
     * @return array
     */
    function getIds($item)
    {
        $ids =  [$item['id']];
        foreach ($item['childs'] as $child) {
            $ids = array_merge($ids, getIds($child));
        }
        return $ids;
    }
}

if (!function_exists('roundPrice')) {

    function roundPrice($price)
    {
        if ($price <= 0) {
            return 0;
        }
        return ceil($price / 1000) * 1000 - 1;
    }
}

if (!function_exists('toNumber')) {
    function toNumber($str = '')
    {
        return  (int) preg_replace('/[^0-9]/', '', $str);
    }
}

if (!function_exists('setCode')) {
    function setCode()
    {
        return  (int) rand(5555, 7777);
    }
}
if (!function_exists('nameGroup')) {
    function nameGroup($count = 0)
    {
        return  (string) 'DATE_' . now()->format('d_m_Y__H_i_s') . '__COUNT_' . $count . '__' . time() . rand(111, 999);;
    }
}


if (!function_exists('gererateEAN128')) {
    function gererateEAN128($i = 0, $prefix = '')
    {
        return $prefix . str_pad(
            $i,
            11,
            '0',
            STR_PAD_LEFT
        );
    }
}

if (!function_exists('optionName')) {
    function optionName($option, $isVendor = false)
    {
        return ($isVendor ? (string) $option->sku . ' | ' : '')
            .  (string) trim(optional(optional($option->product)->brand)->name . ' ' //brand
                . (string) optional(optional($option->product)->name)->title . ' ' //class
                . (string) optional($option->product)->title . ' ' //product name
                . (string) $option->name); //modification name
    }
}
