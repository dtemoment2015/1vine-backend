<?php

namespace App\Http\Helpers;

use App\Http\Resources\v1\Dealer\NavigationResource;
use App\Http\Resources\v1\Dealer\TransactionResouce;
use App\Models\Storage\Block;

class ResourceMap
{

   public static function _get($request, $key = '')
   {
      $user = $request->_user;

      $map = [
         'bottom_bar' => NavigationResource::collection(
            ($user->role && $key == 'bottom_bar') ? $user->role->navigations()->whereHas('blocks', function ($query) {
               $query->whereCode('bottom');
            })->orderBy('position')->get() : []
         ),
         'navigation' => NavigationResource::collection(
            ($user->role && $key == 'navigation') ? $user->role->navigations()->whereHas('blocks', function ($query) {
               $query->whereCode('main');
            })->orderBy('position')->get() : []
         ),
         'transactions' =>  TransactionResouce::collection($user->transactions()->limit(10)->orderBy('id', 'DESC')->get()) // last ten transactions
      ];

      return optional($map)[$key];
   }
}
