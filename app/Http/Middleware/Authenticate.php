<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            header("HTTP/1.1 401 ");
            header('Content-Type: application/json');
            print(json_encode([
                'message' => 'Доступ запрещен / Ruxsat berilmadi',
                'errors' => ['access' => ['Доступ запрещен / Ruxsat berilmadi']]
            ]));
            exit;
        }
    }
}
