<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (optional($request)->localization) {
            $local = $request->localization;
        } else {
            $local = ($request->hasHeader('localization')) ?
                $request->header('localization') :
                'ru';
        }

        $request->setLocale($local);

        app()->setLocale($local);

        $request->request->add(
            [
                '_localization' => $local,
            ]
        );

        return $next($request);
    }
}
