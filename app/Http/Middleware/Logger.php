<?php

namespace App\Http\Middleware;

use App\Jobs\LoggerJob;
use App\Models\Logger as ModelsLogger;
use Closure;
use Illuminate\Http\Request;

class Logger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        $path = $request->path();

        if (
            (
                //$request->isMethod('get')
                 $request->isMethod('put')
                || $request->isMethod('delete')
                 || $request->isMethod('post')
            )
            &&  strpos($path, '_dev') != true
        ) {
            LoggerJob::dispatch(
                $path,
                $request->getMethod(),
               json_encode( $request->all()),
                [], // json_decode($response->getContent()),
                $request->header(),
                $response->status()
            )->onQueue('log');
        }

        return $response;
    }
}
