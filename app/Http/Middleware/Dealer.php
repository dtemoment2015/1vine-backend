<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Dealer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $_user = $request->user();

        if ($request->hasHeader('device_id')) {

            $deviceId = $request->header('device_id');
            $os = $request->header('os');
            $token = $request->header('token');

            $_user->devices()->updateOrCreate(
                [
                    'device_id' => $deviceId,
                    'os' => $os,
                ],
                [
                    'device_id' => $deviceId,
                    'os' => $os,
                    'token' => $token,
                ],
            );
        }

        // if (!$_user->is_active) {
        //     header("HTTP/1.1 403 Forbidden");
        //     header('Content-Type: application/json');
        //     print(json_encode([
        //         'message' => 'Доступ запрещен / Ruxsat berilmadi',
        //         'errors' => ['access' => ['Доступ запрещен / Ruxsat berilmadi']]
        //     ]));
        //     exit;
        // }

        $request->request->add(
            [
                '_user' => $_user->load('cart'),
            ]
        );

        return $next($request);
    }
}
