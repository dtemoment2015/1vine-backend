<?php

namespace App\Http\Requests\v1\Dealer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ActivationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch (request('type')) {
            case 'sale': {
                    $value = 'required|unique:transactions,secret,1,is_cancel,activator_type,dealer';
                    break;
                }
            case 'withdraw': {
                    $value = 'required|numeric|min:1|max:' . request('_user')->transactions()->limit(1)->sum('value');
                    break;
                }
            default: {
                    $value = 'required';
                    break;
                }
        }

        return [
            'type' => 'required|in:sale,withdraw',
            'value' => $value
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()
            ->json(
                [
                    'message' => __('error.form_validate'),
                    'errors' => $validator->errors(),
                ],
                422
            ));
    }
}
