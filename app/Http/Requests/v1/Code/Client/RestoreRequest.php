<?php

namespace App\Http\Requests\v1\Code\Client;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class RestoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|regex:/(?:\+[9]{2}[8][0-9][0-9][0-9]{3}[0-9]{4})/u|string|max:13|exists:clients,phone',
            'hash' => 'required|exists:allowed_phones,hash,phone,' . request('phone'),
            'password' => 'required|string|min:5|confirmed',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'hash.exists' => __('error.hash_not_found'),
            'hash.required' => __('error.hash_not_found'),

            'phone.required' => __('error.phone_format'),
            'phone.regex' => __('error.phone_format'),
            'phone.max' => __('error.phone_format'),
            'phone.string' => __('error.phone_format'),
            'phone.exists' => __('error.phone_not_found'),

            'password.required' => __('error.password_format'),
            'password.string' => __('error.password_format'),
            'password.min' => __('error.password_format'),
            'password.confirmed' => __('error.password_confirmed'),
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [];
    }


    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()
            ->json(
                [
                    'message' => __('error.form_validate'),
                    'errors' => $validator->errors(),
                ],
                422
            ));
    }
}
