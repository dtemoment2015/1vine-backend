<?php

namespace App\Http\Requests\v1\Code\Dealer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class VerifySmsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|regex:/(?:\+[9]{2}[8][0-9][0-9][0-9]{3}[0-9]{4})/u|string|max:13',
            'code' => 'required|exists:codes,code,phone,' . optional($this)->phone
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'phone.required' => __('error.phone_format'),
            'phone.regex' => __('error.phone_format'),
            'phone.max' => __('error.phone_format'),
            'phone.string' => __('error.phone_format'),
            'phone.unique' => __('error.phone_format'),
            'code.required' => __('error.code_format'),
            'code.exists' => __('error.code_not_found'),
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()
            ->json(
                [
                    'message' => __('error.form_validate'),
                    'errors' => $validator->errors(),
                ],
                422
            ));
    }
}
