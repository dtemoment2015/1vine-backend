<?php

namespace App\Http\Requests\v1\Code\Dealer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "first_name" => 'sometimes|required|string|min:1',
            "last_name" => 'sometimes|required|string|min:1',
            "email" => 'nullable|email',
            "password" => 'sometimes|required|string|min:5|confirmed',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'password.required' => __('error.password_format'),
            'password.string' => __('error.password_format'),
            'password.min' => __('error.password_format'),
            'password.confirmed' => __('error.password_confirmed'),
            'first_name.required' => __('error.first_name_format'),
            'first_name.string' => __('error.first_name_format'),
            'first_name.min' => __('error.first_name_format'),
            'last_name.required' => __('error.last_name_format'),
            'last_name.string' => __('error.last_name_format'),
            'last_name.min' => __('error.last_name_format'),
            'email.unique' => __('error.email_exist'),
            'email.email' => __('error.email_format'),
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()
            ->json(
                [
                    'message' => __('error.form_validate'),
                    'errors' => $validator->errors(),
                ],
                422
            ));
    }
}
