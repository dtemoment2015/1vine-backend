<?php

namespace App\Http\Requests\v1\Code\Dealer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class SmsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        request()->request->add(['phone2' => request('phone')]);
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $conditionClientTable = 'unique';
        // if (request('is_restore') == true) {
        //     $conditionClientTable = 'exists';
        // }
        return [
            // 'phone' => 'required|regex:/(?:\+[9]{2}[8][0-9][0-9][0-9]{3}[0-9]{4})/u|' . $conditionClientTable . ':dealers,phone|string|max:13',
            'phone' => 'required|regex:/(?:\+[9]{2}[8][0-9][0-9][0-9]{3}[0-9]{4})/u|unique:codes,phone',
            //'phone2' => 'unique:codes,phone'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'phone.required' => __('error.phone_format'),
            'phone.regex' => __('error.phone_format'),
            'phone.max' => __('error.phone_format'),
            'phone.string' => __('error.phone_format'),
            'phone.exists' => __('error.phone_not_found'),
            'phone.unique' => __('error.phone_code_exist'),
            'phone2.unique' => __('error.phone_code_exist'),
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()
            ->json(
                [
                    'message' => __('error.form_validate'),
                    'errors' => $validator->errors(),
                ],
                422
            ));
    }
}
