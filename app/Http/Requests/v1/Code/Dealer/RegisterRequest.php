<?php

namespace App\Http\Requests\v1\Code\Dealer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|regex:/(?:\+[9]{2}[8][0-9][0-9][0-9]{3}[0-9]{4})/u|string|max:13|unique:dealers,phone',
            'hash' => 'required|exists:allowed_phones,hash,phone,' . request('phone'),
            "first_name" => 'required|string|min:1',
            "last_name" => 'required|string|min:1',
            "email" => 'nullable|email|unique:dealers,email',
            "password" => 'required|string|min:5|confirmed',
            'shop' => 'required|string|min:1'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [

            'hash.exists' => __('error.hash_not_found'),
            'hash.required' => __('error.hash_not_found'),

            'phone.required' => __('error.phone_format'),
            'phone.regex' => __('error.phone_format'),
            'phone.max' => __('error.phone_format'),
            'phone.string' => __('error.phone_format'),
            'phone.unique' => __('error.phone_exist'),

            'password.required' => __('error.password_format'),
            'password.string' => __('error.password_format'),
            'password.min' => __('error.password_format'),
            'password.confirmed' => __('error.password_confirmed'),

            'first_name.required' => __('error.first_name_format'),
            'first_name.string' => __('error.first_name_format'),
            'first_name.min' => __('error.first_name_format'),

            'last_name.required' => __('error.last_name_format'),
            'last_name.string' => __('error.last_name_format'),
            'last_name.min' => __('error.last_name_format'),

            'shop.required' => __('error.shop_format'),
            'shop.string' => __('error.shop_format'),
            'shop.min' => __('error.shop_format'),

            'email.unique' => __('error.email_exist'),
            'email.email' => __('error.email_format'),

        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()
            ->json(
                [
                    'message' => __('error.form_validate'),
                    'errors' => $validator->errors(),
                ],
                422
            ));
    }
}
