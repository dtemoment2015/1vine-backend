<?php

namespace App\Http\Requests\v1\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class DealerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "password" => 'sometimes|required|string|min:5',
            'phone' => 'sometimes|required|regex:/(?:\+[9]{2}[8][0-9][0-9][0-9]{3}[0-9]{4})/u|string|max:13|unique:dealers,phone,' . request('dealer'),
            "llc_name" => 'sometimes|required|string|min:1',
            "inn" => 'sometimes|required|string|min:1|unique:dealers,inn,' . request('dealer'),
            "organizationa_address" => 'sometimes|required',
            "first_name" => 'sometimes|required',
            "last_name" => 'sometimes|required',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'password.required' => __('error.password_format'),
            'password.string' => __('error.password_format'),
            'password.min' => __('error.password_format'),
            'organizationa_address.required' => __('Поле юридический адрес не может быть пустым'),
            'llc_name.required' => __('Поле юридическоое название не может быть пустым'),
            'phone.unique' => __('Данный номер телефона установлен в другом бизнесе'),
            'inn.unique' => __('Данный ИНН установлен в другом бизнесе'),
            'inn.required' => __('Поле ИНН не может быть пустым'),
            'phone.max' => __('Неверный формат у номера телефона. Телефон должен начинаться с "+998"'),
            'phone.string' => __('Неверный формат у номера телефона. Телефон должен начинаться с "+998"'),
            'phone.regex' => __('Неверный формат у номера телефона. Телефон должен начинаться с "+998"'),
            'phone.required' => __('Неверный формат у номера телефона. Телефон должен начинаться с "+998"'),
            'first_name.required' => __('Поле имя  не может быть пустым'),
            'last_name.required' => __('Поле фамилия не может быть пустым'),
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()
            ->json(
                [
                    'message' => __('error.form_validate'),
                    'errors' => $validator->errors(),
                ],
                422
            ));
    }
}
