<?php

namespace App\Http\Controllers\v1\Client\Transaction;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Client\ActivationRequest;
use App\Http\Resources\v1\Client\TransactionCollection;
use App\Http\Resources\v1\Client\TransactionResouce;
use App\Models\Storage\Status;
use App\Models\Warehouse\Sticker;

class IndexController extends Controller
{

    private $item = null;

    private $data = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return
            new TransactionCollection(request()->user()
                ->transactions()
                ->with(['item'])
                ->orderBy('id', 'DESC')
                ->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ActivationRequest $request)
    {
        switch ($request->type) {
            case 'sale': {

                    $this->item = Sticker::whereSecretBuyer($request->value)
                        ->limit(1)
                        ->firstOrFail()
                        ->serial()
                        ->limit(1)
                        ->firstOrFail();

                    $this->data =
                        [
                            'secret' => (string) $request->value,
                            'value' => (float) $this->item->option->reward,
                            'barcode' => (string) $this->item->barcode
                        ];

                    break;
                }

            case 'withdraw': {

                    $this->item = Status::whereCode($request->type)
                        ->limit(1)
                        ->firstOrFail();

                    $this->data =
                        [
                            'value' => (float) abs($request->value) * -1, // принимает только отрицательные значения
                        ];

                    break;
                }
        }

        $transaction =  $request->user()
            ->transactions()
            ->create(
                $this->data
            )
            ->item()
            ->associate($this->item);

        $transaction
            ->save();

        return response()->json(
            new TransactionResouce($transaction)
        );
    }
}
