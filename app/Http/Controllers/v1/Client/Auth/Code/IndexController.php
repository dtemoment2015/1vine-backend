<?php

namespace App\Http\Controllers\v1\Client\Auth\Code;

use App\Http\Controllers\Controller;

use App\Http\Requests\v1\Code\Client\SmsRequest;
use App\Http\Requests\v1\Code\Client\VerifySmsRequest;
use App\Http\Resources\v1\Client\ClientResource;
use App\Jobs\v1\RemoveObjectJob;
use App\Jobs\v1\SmsJob;

use App\Models\Confirmation\Code;
use App\Models\User\Client;

class IndexController extends Controller
{
    public function index(SmsRequest $request)
    {
        $query = Code::updateOrCreate(
            ['phone' => $request->phone],
            [
                'phone' => $request->phone,
                'code' => $devCode =  setCode()
            ]
        );

        SmsJob::dispatch($query, __(
            'client.system.sms_code',
            ['code' => $query->code]
        ))->onQueue('sms');

        RemoveObjectJob::dispatch($query)
            ->delay(
                now()
                    ->addMinutes(
                        Code::MINUTE_DESTROY
                    )
            )->onQueue('sms');

        $query->TEMP_DEV_CODE = $devCode;

        return response()->json(
            $query
        );
    }

    public function verify(VerifySmsRequest $request)
    {
        $user = Client::updateOrCreate(
            [
                'phone' => $request->phone
            ]
        );

        Code::wherePhone($request->phone)->delete();

        $token = $user->createToken('Client');

        return response()->json(new ClientResource($user))->withHeaders([
            'X-Access-Token' => optional($token)->accessToken,
            'X-Expires-At' => optional(optional($token)->token)['expires_at']
        ]);
    }
}
