<?php

namespace App\Http\Controllers\v1\Client\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Code\Client\ActivateRequest;
use App\Http\Requests\v1\Code\Client\UpdateRequest;
use App\Http\Resources\v1\Client\ClientResource;

class IndexController extends Controller
{

    public function index()
    {
        return response()->json(
            new ClientResource(request()->user())
        );
    }

    public function update(UpdateRequest $request)
    {
        $user = $request->user();

        $user->update($request->only([
            'first_name',
            'last_name',
            'email',
            'phone',
            'password',
            'birthday',
        ]));
        return response()->json(
            new ClientResource($user)
        );
    }
}
