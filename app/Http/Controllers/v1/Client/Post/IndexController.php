<?php

namespace App\Http\Controllers\v1\Client\Post;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Client\PostCollection;
use App\Http\Resources\v1\Client\PostResource;
use App\Models\Storage\Post;


class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new  PostCollection(
            Post::whereIsActive(true)
                ->orderBy('id', 'DESC')
                ->paginate()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        return response()->json(
            new PostResource(Post::whereId($id)
                ->with('contents')
                ->whereIsActive(true)
                ->firstOrFail())
        );
    }
}
