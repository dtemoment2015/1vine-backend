<?php

namespace App\Http\Controllers\v1\Dealer\Dev;

use App\Http\Controllers\Controller;
use App\Models\Confirmation\Code;
use App\Models\Logger;
use App\Models\User\Dealer;

class IndexController extends Controller
{

    public function dealerRemove()
    {
        $user = Dealer::wherePhone(request('phone'))->firstOrFail();

        $user->tokens->each(function ($token, $key) {
            $token->delete();
        });

        return response()->json(
            (bool) $user->delete()
        );
    }

    public function dealerDeactive()
    {
        $user = Dealer::wherePhone(request('phone'))->firstOrFail();
        $user->is_active = false;
        $user->save();
        $temp = $user->dealer_temp()->firstOrCreate();
        $temp->code = (string) setCode();
        $temp->address = 'TEMP SHOP';
        $temp->save();

        return response()->json(
            $temp
        );
    }

    public function dealerActivation()
    {
        return response()->json(
            Dealer::wherePhone(request('phone'))
                ->firstOrFail()
                ->dealer_temp()
                ->firstOrFail()
        );
    }

    public function smsReset()
    {
        return response()->json(
            ['is_remove' => (bool) Code::wherePhone(request('phone'))->delete()]
        );
    }


    public function loggerList()
    {
        return response()->json(
            Logger::when(request('status'), function ($query) {
                $query->whereStatus(request('status'));
            })
                ->when(request('uri'), function ($query) {
                    $query->where('uri', 'like', '%' . request('uri') . '%');
                })
                ->orderBy('id', 'DESC')
                ->paginate()
        );
    }

    public function loggerClear()
    {
        return response()->json(
            ['is_remove' => (bool) Logger::truncate()]
        );
    }
}
