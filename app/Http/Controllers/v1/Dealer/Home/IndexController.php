<?php

namespace App\Http\Controllers\v1\Dealer\Home;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Dealer\NavigationResource;
use App\Models\Storage\Navigation;

class IndexController extends Controller
{
    public function index(Navigation $navigation)
    {
        return response()->json(
            new  NavigationResource(
                $navigation
                    ->whereCode('home')
                    ->with('sections')
                    ->firstOrFail()
            )
        );
    }
}
