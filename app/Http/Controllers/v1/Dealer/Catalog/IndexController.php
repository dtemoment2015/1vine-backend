<?php

namespace App\Http\Controllers\v1\Dealer\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Dealer\CategoryCollection;
use App\Http\Resources\v1\Dealer\CategoryResource;
use App\Models\Warehouse\Category;

class IndexController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category)
    {
        return response()
            ->json(
                new CategoryCollection(
                    $category->with('childs')
                        ->whereIsActive(true)
                        ->whereParentId('>', 0)
                        ->get()
                )
            );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category, $id)
    {
        return response()->json(
            new CategoryResource(
                $category
                    ->withCount('options as count')
                    ->whereId($id)
                    ->limit(1)
                    ->firstOrFail()
            )
        );
    }
}
