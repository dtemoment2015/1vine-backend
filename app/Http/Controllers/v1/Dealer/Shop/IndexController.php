<?php

namespace App\Http\Controllers\v1\Dealer\Shop;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Dealer\ShopAddRequest;
use App\Http\Resources\v1\Dealer\ShopResource;
use Exception;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return
            ShopResource::collection(request('_user')
                ->shops()
                ->with('media')
                ->orderBy('id', 'DESC')
                ->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ShopAddRequest $request)
    {

        $Item = $request->_user->shops()->create($request->all());

        try {
            if (($images = optional($request)->media_new) &&
                is_array($images) &&
                count($images)
            ) {
                foreach ($images as $image) {
                    $Item->addMedia($image)->toMediaCollection('shops');
                }
            }
        } catch (Exception $e) {
            return response()->json([
                'message' =>  $e->getMessage(),
                'errors' => ['media_new' => [__('Ошибка формата файла')]],
            ], 422);
        }
        
        return new ShopResource(
            $Item->load(['location', 'media'])
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item =  request('_user')
            ->shops()
            ->with('media')
            ->whereId($id)
            ->firstOrFail();

        return new ShopResource(
            $item
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $item = $request->_user
            ->shops()
            ->whereId($id)
            ->firstOrFail();

        try {

            if (($images = optional($request)->media_new) &&
                is_array($images) &&
                count($images)
            ) {
                foreach ($images as $image) {
                    $item->addMedia($image)->toMediaCollection('shops');
                }
            }
        } catch (Exception $e) {
            return response()->json([
                'message' =>  $e->getMessage(),
                'errors' => ['media_new' => [__('Ошибка формата файла')]],
            ], 422);
        }


        if ($images_delete = optional($request)->media_destroy) {
            foreach ($images_delete as $image) {
                try {
                    $item->deleteMedia($image);
                } catch (Exception $e) {
                }
            }
        }

        $item->update($request->all());

        return new ShopResource(
            $item->load(['location', 'media'])
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json([
            'deleted' => (bool)  request('_user')
                ->shops()
                ->whereId($id)
                ->delete()
        ]);
    }
}
