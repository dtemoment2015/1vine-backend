<?php

namespace App\Http\Controllers\v1\Dealer\Storage;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Dealer\TextCollection;
use App\Http\Resources\v1\LocationResource;
use App\Models\Cloud\Location;
use App\Models\Text;

use Illuminate\Support\Facades\Cache;

class IndexController extends Controller
{
    public function data()
    {
        return response()->json(
            array_merge(
                Cache::rememberForever('text-' . request('_localization'),  function () {
                    return  Text::get()->mapToGroups(function ($item) {
                        return [$item->group => [$item->key => $item->title]];
                    })
                        ->map(function ($i) {
                            return $i->collapse();
                        })
                        ->toArray();
                }),
                ['locale' => config('translatable.localizations')]
            )
        );
    }

    public function regions()
    {
        return   LocationResource::collection(
            Cache::rememberForever('location-' . request('_localization'), function () {
                return Location::whereParentId('>', 0)->with('childs')->get();
            })
        );
    }
}
