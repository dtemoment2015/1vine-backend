<?php

namespace App\Http\Controllers\v1\Dealer\Order;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Dealer\CreateOrderRequest;
use App\Http\Resources\v1\Dealer\OrderCollection;
use App\Http\Resources\v1\Dealer\OrderResource;
use App\Models\Storage\Status;
use App\Models\Warehouse\Option;
use Exception;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new  OrderCollection(
            request('_user')
                ->orders()
                ->price()
                ->dateFilter()
                ->orderBy('id', 'DESC')
                ->paginate()
        );
    }

    public function store(CreateOrderRequest $request)
    {

        $item =  $request->_user
            ->orders()->create(
                [
                    'code' => time()
                ]
            );

        if (!$shop =  $request->_user->shops()->whereId($request->shop_id)->first()) {
            return response()->json([
                'message' => __('error.shop_error'),
                'errors' => ['object' => [__('error.shop_error')]]
            ], 422);
        }

        $item->customer()->create(
            [
                'name' => $request->_user->last_name . ' ' . $request->_user->first_name,
                'phone' => $request->_user->phone,
                'address' => $shop->address,
                'location_id' => $shop->location_id,
                'shop_id' => $shop->id,
            ]
        );

        $status = $item->statuses()->create([
            'status_id' => Status::whereCode('accept')
                ->firstOrFail()->id
        ]);

        $item->order_status_id = $status->id;  
        $item->save();


        try {
            $purchases = Option::whereIn('id', array_keys($request->purchases))->get()->map(function ($option) use ($request) {
                $qty =  (int) optional($request->purchases)[$option->id];
                return [
                    'display_name' => optionName($option),
                    'price' => $option->price,
                    'sku' => (string) $option->sku,
                    'price' => $option->price,
                    'reward' => (float) $option->reward,
                    'option_id' => $option->id,
                    'price_old' => $option->price_old,
                    'quantity' => $qty,
                    'image' => optional(optional($option->images)[0])->file  ?  url('storage/options/x600/' . $option->images[0]->file) : "",
                ];
            });
            
            $item->purchases()->createMany($purchases->where('quantity', '>', '0'));

        } catch (Exception $e) {
        }

        return new OrderResource($item->load('purchases'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = request('_user')
            ->orders()
            ->price()
            ->with('purchases')
            ->whereId($id)
            ->limit(1)
            ->firstOrFail();

        $order->status_list = Status::whereIsActive(true)
            ->whereType('order')
            ->get();

        return response()->json(
            new OrderResource($order)
        );
    }
}
