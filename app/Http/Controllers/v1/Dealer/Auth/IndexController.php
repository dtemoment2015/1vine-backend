<?php

namespace App\Http\Controllers\v1\Dealer\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Code\Dealer\LoginRequest;
use App\Http\Requests\v1\Code\Dealer\RegisterRequest;
use App\Http\Requests\v1\Code\Dealer\RestoreRequest;
use App\Http\Resources\v1\Dealer\DealerResource;
use App\Models\Confirmation\AllowedPhone;
use App\Models\Confirmation\Code;
use App\Models\User\Dealer;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class IndexController extends Controller
{
    
    public function login(LoginRequest $request)
    {
        $user = Dealer::wherePhone($request->phone)
            ->limit(1)
            ->firstOrFail();

        if (Hash::check(
            $request->password,
            $user->password
        )) {
            $token = $user->createToken('DEALER');

            return response()->json(new DealerResource($user))->withHeaders([
                'X-Access-Token' => optional($token)->accessToken,
                'X-Expires-At' => optional(optional($token)->token)['expires_at']
            ]);
        } else {
            return response()->json(
                [
                    'message' => __('error.form_validate'),
                    'errors' => [
                        'phone' => [__('auth.access')]
                    ]
                ],
                422
            );
        }
    }

    public function register(RegisterRequest $request)
    {
        try {

            $user = DB::transaction(function () use ($request) {
                $user = Dealer::create($request->only([
                    'first_name',
                    'last_name',
                    'email',
                    'phone',
                    'password',
                    'email',
                ]));

                $temp = $user->dealer_temp()->firstOrCreate();
                $temp->code = setCode();
                $temp->address = $request->shop;
                $temp->save();
                $this->reset($request->phone);
                return $user;
            });

            if ($user) {
                $token = $user->createToken('DEALER');
                return response()->json(new DealerResource($user))->withHeaders([
                    'X-Access-Token' => optional($token)->accessToken,
                    'X-Expires-At' => optional(optional($token)->token)['expires_at']
                ]);
            } else {
                throw new Exception;
            }
        } catch (Exception $e) {
            return response()->json([
                'message' => __('error.form_validate'),
                'errors' => [
                    'system' => $e->getMessage()
                ]
            ], 422);
        }
    }

    public function restore(RestoreRequest $request)
    {

        $user = Dealer::wherePhone($request->phone)->limit(1)->firstOrFail();
        $user->password = $request->password;
        $user->save();
        $this->reset($request->phone);
        $token = $user->createToken('DEALER');
        return (new DealerResource($user))
            ->response()
            ->header('X-Access-Token',  optional($token)->accessToken)
            ->header('X-Expires-At', optional(optional($token)->token)['expires_at']);
    }

    protected function reset($phone = null): bool
    {
        return Code::wherePhone($phone)->delete() &&
            AllowedPhone::wherePhone($phone)->delete();
    }
}
