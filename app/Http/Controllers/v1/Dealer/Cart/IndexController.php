<?php

namespace App\Http\Controllers\v1\Dealer\Cart;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Dealer\CartAddRequest;
use App\Http\Requests\v1\Dealer\CartCheckoutRequest;
use App\Http\Resources\v1\Dealer\CartCollection;
use App\Http\Resources\v1\Dealer\OrderResource;
use App\Models\Storage\Status;
use App\Models\Warehouse\Option;
use Exception;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    
    public function index()
    {
        return new CartCollection(
            request('_user.cart')
        );
    }

    public function add(CartAddRequest $request)
    {
        if (!Option::stock()
            ->whereId($request->option_id)
            ->where('quantity', '>=', $request->count)
            ->limit(1)
            ->exists()) {
            return response()->json([
                'message' => __('error.form_validate'),
                'errors' => ['add' => [__('error.no_product')]]
            ], 404);
        }

        $cart = request('_user')->cart();

        $cart->syncWithoutDetaching(
            [
                $request->option_id => [
                    'count' => $request->count
                ]
            ]
        );

        return new CartCollection(
            $cart->get()
        );
    }

    public function delete($id = null)
    {
        $cart = request('_user')->cart();

        $cart->detach([$id]);
        return new CartCollection(
            $cart->get()
        );
    }

    public function checkout(CartCheckoutRequest $request)
    {

        $user =   $request->_user;

        $cart =  $user->cart;

        if (!$cart->count()) {
            return response()->json([
                'message' => __('error.form_validate'),
                'errors' => [
                    'cart' => [__('error.empty_cart')]
                ]
            ], 422);
        }
        try {
            $order = DB::transaction(function () use ($user, $cart, $request) {
                
                $status = Status::whereType('order')->whereIsActive(true)->get();

                $order =  $user->orders()->create();

                //add status
                $createStatus = $order->statuses()->create(
                    [
                        'status_id' => optional($status
                            ->where('code', Status::START_STATUS)
                            ->first())
                            ->id,
                    ]
                );
                
                $order->order_status_id = $createStatus->id;  
                
                $order->save();


                //generate address
                if (!$shop = $user->shops()->whereId($request->shop_id)->first()) {
                    return false;
                }
                
                //add customer
                $order->customer()->create([
                    'name' => $user->full_name,
                    'phone' => $user->phone,
                    'address' =>  $shop ? optional(optional($shop->location)->parent)->title . ' ' . optional($shop->location)->title  . ' ' .  $shop->address : "" ,
                    'location_id' =>  optional($shop)->location_id,
                    'shop_id' => $shop->id
                ]);

                //add purchses
                $order->purchases  =   $order->purchases()->createMany($cart->map(function ($option) {
                    return [
                        'display_name' => optionName($option),
                        'price' => $option->price,
                        'sku' => (string) $option->sku,
                        'price' => $option->price,
                        'reward' => (float) $option->reward,
                        'option_id' => $option->id,
                        'price_old' => $option->price_old,
                        'quantity' => $option->pivot->count,
                        'image' => optional(optional($option->images)[0])->file  ?  url('storage/options/x600/' . $option->images[0]->file) : "",
                    ];
                }));

                $order->status_list = $status;

                $user->cart()->detach();

                return $order->load('purchases');

            });

            if ($order) {
            } else {
                return response()->json([
                    'message' => __('error.form_validate'),
                    'errors' => [
                        'system' => [__('Магазин выбран не ваш')]
                    ]
                ], 422);
            }
        } catch (Exception $e) {
            return response()->json([
                'message' => __('error.form_validate'),
                'errors' => [
                    'system' => $e->getMessage()
                ]
            ], 422);
        }

        return response()->json(
            new  OrderResource($order)
        );
    }
}
