<?php

namespace App\Http\Controllers\v1\Dealer\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Code\Dealer\ActivateRequest;
use App\Http\Requests\v1\Code\Dealer\UpdateRequest;
use App\Http\Resources\v1\Dealer\DealerResource;

class IndexController extends Controller
{

    public function index()
    {
        return response()->json(
            new DealerResource(request()->user())
        );
    }

    public function update(UpdateRequest $request)
    {
        $user = $request->user();

        $user->update($request->only([
            'first_name',
            'last_name',
            'email',
            'phone',
            'password',
            'email',
        ]));
        return response()->json(
            new DealerResource($user)
        );
    }

    public function activate(ActivateRequest $request)
    {

        $user = $request->user();

        $temp = $user->dealer_temp()->first();

        if (!$temp) {
            return response()->json([
                'message' => __('error.form_validate'),
                'errors' => ['code' => [__('error.code_already_activated')]]
            ], 422);
        }

        if ($request->code != $temp->code) {
            return response()->json([
                'message' => __('error.form_validate'),
                'errors' => ['code' => [__('error.code_error')]]
            ], 422);
        }

        $user->is_active = true;
        $user->save();
        $temp->delete();

        return response()->json(
            new DealerResource($user)
        );
    }
}
