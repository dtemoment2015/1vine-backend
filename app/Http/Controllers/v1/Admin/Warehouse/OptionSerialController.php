<?php

namespace App\Http\Controllers\v1\Admin\Warehouse;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Admin\Warehouse\SerialAddRequest;
use App\Http\Resources\v1\Admin\SerialResource;
use App\Models\Warehouse\Option;


class OptionSerialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($optionId = null)
    {
        return  SerialResource::collection(
            Option::whereId($optionId)
                ->firstOrFail()
                ->serials()
                ->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($optionId = null, SerialAddRequest $request)
    {
        return new  SerialResource(
            Option::whereId($optionId)
                ->firstOrFail()
                ->serials()
                ->create($request->all())
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($optionId, $id)
    {
        return response()->json(
            ['is_delete' =>   (bool)
            Option::whereId($optionId)
                ->firstOrFail()
                ->serials()
                ->whereId($id)
                ->delete()]
        );
    }
}
