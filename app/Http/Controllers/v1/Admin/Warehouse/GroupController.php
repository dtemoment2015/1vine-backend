<?php

namespace App\Http\Controllers\v1\Admin\Warehouse;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\ProductAdminResource;
use App\Models\Warehouse\Product;
use Illuminate\Http\Request;

class GroupController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return  new ProductAdminResource(
            Product::whereHas('options', function($query) use($id){
                $query->whereId($id);
            })
            ->with('options')
                ->limit(1)
                ->firstOrFail()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Product::
            whereId($id)
            ->limit(1)
            ->firstOrFail();

        $item->update($request->all());

        $item->categories()->sync(
            (array) optional($request)->categories
        );
        
        return  new ProductAdminResource(
            $item
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
