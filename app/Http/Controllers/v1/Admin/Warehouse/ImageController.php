<?php

namespace App\Http\Controllers\v1\admin\warehouse;

use App\Http\Controllers\Controller;
use App\Http\Resources\ImageResource;
use App\Http\Resources\v1\Admin\OptionAdminResource;
use App\Models\Warehouse\Option;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($optionId = null)
    {
        return new  OptionAdminResource(
            Option::whereId($optionId)
                ->firstOrFail()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($optionId = null, Request $request)
    {
        $images = collect((array)optional($request)->images)
            ->map(function ($item) {
                return [
                    'file' => $item,
                    'position' => 0
                ];
            });

        $option =   Option::whereId($optionId)
            ->firstOrFail();

        $option->images()
            ->createMany($images);

        return  ImageResource::collection(
            $option->images()->get()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($optionId = null, $id)
    {
        return response()->json([
            'id' => (int) $id,
            'deleted' => (bool)  Option::whereId($optionId)
                ->limit(1)
                ->firstOrFail()
                ->images()
                ->whereId($id)
                ->delete()
        ]);
    }
}
