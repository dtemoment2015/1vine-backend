<?php

namespace App\Http\Controllers\v1\Admin\Warehouse;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\FeatureAdminResource;
use App\Models\Warehouse\Feature;
use Illuminate\Http\Request;

class SubFeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($featureId = null)
    {
        return  FeatureAdminResource::collection(
            Feature::whereId($featureId)
                ->limit(1)
                ->whereParentId(0)
                ->firstOrFail()
                ->childs()
                ->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $featureId = null)
    {
        return new  FeatureAdminResource(
            Feature::whereId($featureId)
                ->limit(1)
                ->firstOrFail()
                ->childs()
                ->create(
                    $request->all()
                )
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($featureId, $id)
    {
        return response()->json(
            [
                'id' => (int) $id,
                'deleted' => (bool)  Feature::whereId($featureId)
                    ->limit(1)
                    ->firstOrFail()
                    ->childs()
                    ->whereId($id)
                    ->delete()
            ]
        );
    }
}
