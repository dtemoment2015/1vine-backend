<?php

namespace App\Http\Controllers\v1\Admin\Warehouse;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\ValueAdminResource;
use App\Models\Warehouse\Value;
use Illuminate\Http\Request;

class ValueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  ValueAdminResource::collection(
            Value::when(request('search'), function ($query) {
                $query->search(request('search'))
                    ->orderBy('relevance', 'DESC');
            })
                ->groupBy('id')
                ->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = Value::create(
            $request->all()
        );

        return  new ValueAdminResource(
            $item
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return  new ValueAdminResource(
            Value::whereId($id)
                ->limit(1)
                ->firstOrFail()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Value::whereId($id)
            ->limit(1)
            ->firstOrFail();

        $item->update($request->all());

        return  new ValueAdminResource(
            $item
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(
            [
                'id' => (int) $id,
                'deleted' => (bool)  Value::whereId($id)
                    ->limit(1)
                    ->delete()
            ]
        );
    }
}
