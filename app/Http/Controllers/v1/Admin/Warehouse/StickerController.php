<?php

namespace App\Http\Controllers\v1\Admin\Warehouse;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Admin\Warehouse\StickerAddRequest;
use App\Http\Resources\v1\Admin\StickerAdminResource;
use App\Models\Warehouse\Sticker;
use Illuminate\Http\Request;

class StickerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  StickerAdminResource::collection(
            Sticker::with('serial')->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $count = (int) optional($request)->count;
        $group = nameGroup($count);
        for ($i = 0; $i <  $count; $i++) {
            $sticker =  Sticker::create(
                ['group' =>  $group]
            );
            $sticker->barcode = gererateEAN128($sticker->id);
            $sticker->save();
        }
        return  response()->json(['file' => null]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return  new StickerAdminResource(
            Sticker::whereId($id)
                ->with('serial')
                ->limit(1)
                ->firstOrFail()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StickerAddRequest $request, $id)
    {
        $item = Sticker::whereId($id)
            ->limit(1)
            ->firstOrFail();

        $item->update($request->all());

        return  new StickerAdminResource(
            $item
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(
            [
                'id' => (int) $id,
                'deleted' => (bool)  Sticker::whereId($id)
                    ->limit(1)
                    ->delete()
            ]
        );
    }
}
