<?php

namespace App\Http\Controllers\v1\Admin\Warehouse;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\NameAdminResource;
use App\Models\Warehouse\Name;
use Illuminate\Http\Request;

class NameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  NameAdminResource::collection(
            Name::when(request('search'), function ($query) {
                    $query->search(request('search'))
                        ->orderBy('relevance', 'DESC');
                })
                ->groupBy('id')
                ->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = Name::create(
            $request->all()
        );

        return  new NameAdminResource(
            $item
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return  new NameAdminResource(
            Name::whereId($id)
                ->limit(1)
                ->firstOrFail()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $item = Name::whereId($id)
            ->limit(1)
            ->firstOrFail();

        $item->update($request->all());

        return  new NameAdminResource(
            $item
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(
            [
                'id' => (int) $id,
                'deleted' => (bool)  Name::whereId($id)
                    ->limit(1)
                    ->delete()
            ]
        );
    }
}
