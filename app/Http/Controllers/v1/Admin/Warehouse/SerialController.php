<?php

namespace App\Http\Controllers\v1\Admin\Warehouse;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Admin\Warehouse\SerialAddRequest;
use App\Http\Resources\v1\Admin\SerialResource;
use App\Models\Warehouse\Serial;
use Illuminate\Http\Request;

class SerialController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  SerialResource::collection(
            Serial::with('option')->paginate()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = null)
    {
        return new  SerialResource(
            Serial::with('option')->whereId($id)
                ->firstOrFail()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SerialAddRequest $request, $id)
    {
        $serial =  Serial::limit(1)
            ->whereId($id)
            ->firstOrFail();
            
        $serial->update($request->all());

        return new  SerialResource(
            $serial
        );
    }
}
