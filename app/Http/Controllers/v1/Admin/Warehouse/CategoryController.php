<?php

namespace App\Http\Controllers\v1\Admin\Warehouse;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\CategoryAdminResource;
use App\Models\Warehouse\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($categoryId = null)
    {
        return  CategoryAdminResource::collection(
            Category::when(request('search'), function ($query) {
                    $query->search(request('search'))
                        ->orderBy('relevance', 'DESC');
                })
                ->with('parent')
                ->when($categoryId, function ($query) use ($categoryId) {
                    $query->whereHas('parent', function ($query) use ($categoryId) {
                        $query->whereId($categoryId);
                    });
                })
                ->groupBy('id')
                ->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $categoryId = null)
    {
        if ($categoryId) {
            $item = Category::whereId($categoryId)
                ->firstOrFail()
                ->childs()
                ->create($request->only(['title:ru', 'title:uz', 'parent_id', 'is_active']));
        } else {
            $item =  Category::create(
                $request->only(['title:ru', 'title:uz', 'parent_id', 'is_active'])
            );
        }

        return  new CategoryAdminResource(
            $item
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return  new CategoryAdminResource(
            Category::whereId($id)
                ->limit(1)
                ->firstOrFail()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Category::whereId($id)
            ->limit(1)
            ->firstOrFail();

        $item->update(
            $request->only(['title:ru', 'title:uz', 'parent_id', 'is_active'])
        );

        return  new CategoryAdminResource(
            $item
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(
            [
                'id' => (int) $id,
                'deleted' => (bool)  Category::whereId($id)
                    ->limit(1)
                    ->delete()
            ]
        );
    }
}
