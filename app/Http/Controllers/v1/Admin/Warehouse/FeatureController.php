<?php

namespace App\Http\Controllers\v1\Admin\Warehouse;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\FeatureAdminResource;
use App\Models\Warehouse\Feature;
use App\Models\Warehouse\Name;
use Illuminate\Http\Request;

class FeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($nameId = null)
    {
        return  FeatureAdminResource::collection(
            Feature::
            when(request('search'), function ($query) {
                $query->search(request('search'))
                    ->orderBy('relevance', 'DESC');
            })
            ->with('name', 'childs', 'parent')
                ->when($nameId, function ($query) use ($nameId) {
                    $query->whereHas('name', function ($query) use ($nameId) {
                        $query->whereId($nameId);
                    });
                })
                ->whereDoesntHave('parent')
                ->groupBy('id')
                ->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($nameId = null, Request $request)
    {
        if ($nameId) {
            $item  = Name::whereId($nameId)->firstOrFail()->features()->create(
                $request->all()
            );
        } else {
            $item = Feature::create(
                $request->all()
            );
        }
        return  new FeatureAdminResource(
            $item
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return  new FeatureAdminResource(
            Feature::with('parent', 'name')
                ->whereId($id)
                ->limit(1)
                ->firstOrFail()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Feature::with('parent', 'name')
            ->whereId($id)
            ->limit(1)
            ->firstOrFail();

        $item->update($request->all());

        return  new FeatureAdminResource(
            $item
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(
            [
                'id' => (int) $id,
                'deleted' => (bool)  Feature::whereId($id)
                    ->limit(1)
                    ->delete()
            ]
        );
    }
}
