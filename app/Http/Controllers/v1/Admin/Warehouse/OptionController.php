<?php

namespace App\Http\Controllers\v1\Admin\Warehouse;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\OptionAdminResource;
use App\Models\Warehouse\Option;
use App\Models\Warehouse\Value;
use Illuminate\Http\Request;

class OptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  OptionAdminResource::collection(
            Option::filter()
                ->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = Option::create(
            $request->only([
                'product_id',
                'sku',
                'price',
                'quantity',
                'reward',
                'name',
                'is_active',
                'price_old',
                'price_retail'
            ])
        );

        // $item->categories()->sync(
        //     (array) optional($request)->categories
        // );

        return  new OptionAdminResource(
            $item
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return  new OptionAdminResource(
            Option::whereId($id)
                ->with([
                    'modifications',
                    'values',
                    'product.name.features.childs',
                    'contents',
                    // 'categories'
                ])
                ->limit(1)
                ->firstOrFail()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Option::whereId($id)
            ->with('contents')
            ->limit(1)
            ->firstOrFail();

        // $item->categories()->sync(
        //     (array) optional($request)->categories
        // );

        if (
            optional($request)->is_feature
            && is_array(optional($request)->values)
        ) {
            $this->features($request, $item);
        } elseif (
            optional($request)->is_content
            && is_array(optional($request)->contents)
        ) {
            $item->contents =  $this->contents($request, $item);
        } else {
            $item->update($request->all());
        }

        return  new OptionAdminResource(
            $item
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(
            [
                'id' => (int) $id,
                'deleted' => (bool)  Option::whereId($id)
                    ->limit(1)
                    ->delete()
            ]
        );
    }

    private function contents($request = null, $item = null)
    {
        if ($request && $item) {

            $contents = collect($request->contents);

            $contents->where('is_remove', false)->map(function ($i, $k) use ($item) {
                if ($i) {

                    if (optional($i)['id']) {
                        optional($item->contents()
                            ->whereId($i['id'])->first())
                            ->update($i);
                    } else {
                        $i['position'] = time() + $k;

                        $i['id']  = optional($item
                            ->contents()
                            ->create(
                                $i
                            ))->id;
                    }
                }
                return $i;
            });

            $contents->where('is_remove', true)->map(function ($i) use ($item) {
                $item->contents()
                    ->whereId(optional($i)['id'])
                    ->delete();
                return $i;
            });
        }

        return $item->contents()->get();
    }

    private function features($request = null, $item = null)
    {
        if ($request && $item) {
            foreach ($request->values as $k => $value) {
                $featureId = toNumber($k);
                $val = collect($value)->map(function ($item) {
                    $item['id'] = !$item['id'] ?  Value::create(['title' => $item['title']])->id : $item['id'];
                    return $item;
                });
                $item->values()->wherePivot('feature_id', $featureId)->sync(
                    $val->whereNotNull('id')->mapWithKeys(function ($item) use ($featureId) {
                        return  [
                            $item['id'] => ['feature_id' => $featureId],
                        ];
                    })
                );
            }
        }

        return true;
    }
}
