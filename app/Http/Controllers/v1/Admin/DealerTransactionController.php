<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\TranslationAdminResource;
use App\Models\Storage\Status;
use App\Models\User\Dealer;
use App\Models\User\Transaction;
use Illuminate\Http\Request;

class DealerTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($dealerId = null)
    {
        return  TranslationAdminResource::collection(
            Dealer::whereId($dealerId)
                ->firstOrFail()
                ->transactions()
                ->orderBy('id', 'DESC')
                ->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($dealerId = null, Request $request)
    {
        $value =  (int) $request->value;

        $status =  Status::whereId($request->status_id)->firstOrFail();

        if ($status->code == 'withdraw') {
            $value = abs($value) * -1;
        }
        
        $transaction =   Dealer::whereId($dealerId)
            ->firstOrFail()
            ->transactions()
            ->create(
                [
                    'value' => $value
                ]
            );

        $transaction->item()->associate($status);

        $transaction->user_id = $request->user()->id;

        $transaction->save();

        return new TranslationAdminResource(
            $transaction
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($dealerId = null, $id)
    {
        return response()->json(
            [
                'is_deleted' => (bool)  Transaction::whereId($id)
                    ->delete()
            ]
        );
    }
}
