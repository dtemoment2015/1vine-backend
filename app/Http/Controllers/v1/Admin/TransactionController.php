<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\TranslationAdminResource;
use App\Models\User\Dealer;
use App\Models\User\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($dealerId = null)
    {
        return  TranslationAdminResource::collection(
            Transaction::orderBy('id', 'DESC')->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return new TranslationAdminResource(
        //     Transaction::create($request->all())
        // );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        return new TranslationAdminResource(
           $transaction
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $value =  (float) $request->value;

        $item = Transaction::whereId($id)
            ->firstOrFail();

        if ($item->item->code == 'withdraw')      {
            $value =abs($value)*-1;
        }

        $item->update([
            'value' => $value ,
            'is_confirm' => (bool) $request->is_confirm,
         ]);

        return new TranslationAdminResource(
            $item
        );
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return     response()->json([
                'is_deleted' => (bool)  Transaction::whereId($id)
                    ->delete()
            ]
        );
    }
}
