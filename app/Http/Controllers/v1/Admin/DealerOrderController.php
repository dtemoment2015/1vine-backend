<?php

namespace App\Http\Controllers\v1\Admin;

use App\Events\v1\Dealer\OrderEvent;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\OrderAdminResource;
use App\Http\Resources\v1\Dealer\OrderResource;
use App\Models\User\Dealer;
use App\Models\Order\Order;
use Illuminate\Http\Request;

class DealerOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Dealer $dealer)
    {
        return  OrderAdminResource::collection(

            $dealer->orders()->with(['dealer','user', 'status'])
                ->when(request('search'), function ($query) {
                    $query->search(request('search'))
                        ->orderBy('relevance', 'DESC');
                })
                ->dateFilter()
             
                ->when(request('status_order_id'), function ($query) {
                    $query->whereHas('status.status', function ($query) {
                        $query->whereIn('id', explode('-', request('status_order_id')));
                    });
                })
                ->when(request('role'), function ($query) {
                    $query->whereHas('dealer.role', function ($query) {
                        $query->whereCode(request('role'));
                    });
                })
                ->when(request('is_paid'), function ($query) {
                    $query->whereIsPaid((int) request('is_paid') - 1);
                })
                // ->price()
                ->when(request('order'), function ($query) {
                    $order = request('order');
                    $query
                        ->when($order == 'id', function ($query) {
                            $query->orderByDesc('orders.id');
                        })->when($order == '-id', function ($query) {
                            $query->orderBy('orders.id');
                        });
                })
                ->orderByDesc('orders.id')
                ->groupBy('orders.id')
                ->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Dealer $dealer)
    {
        $item =   $dealer->orders()->create([
            'code' => time(),
            'dealer_id' => $request->dealer_id
        ]);

        $dealer = Dealer::whereId($request->dealer_id)->firstOrFail();

        $shop = $dealer->shops()->whereId($request->shop_id)->firstOrFail();

        $item->customer()->create(
            [
                'name' => $dealer->last_name . ' ' . $dealer->first_name,
                'phone' => $dealer->phone,
                'address' => $shop->address,
                'location_id' => $shop->location_id,
                'shop_id' => $shop->id,
            ]
        );

        $item->status()->create(['status_id' => $request->status_id]);

        return new OrderAdminResource(
            $item
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Dealer $dealer, $id)
    {
        return new OrderAdminResource(
            $dealer->orders()->whereId($id)
                ->with('customer.shop')
                ->price()
                ->reward()
                ->firstOrFail()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dealer $dealer, $id)
    {

        $item =    $dealer->orders()->whereId($id)
            ->firstOrFail();

        $statusId = optional($request)->status_id;

        $item->update($request->all());

        if (($statusId != optional($item->status)->status_id) || !$item->status) {
          $status =  $item->statuses()
                ->create(
                    [
                        'status_id' => $statusId,
                        'user_id' => $request->user()->id
                    ]
                );
                
              $item->order_status_id = $status->id;  
              $item->save();

        }

        $order =   new OrderResource($item->load(['purchases']));

        event(
            new OrderEvent(
                $order
            )
        ); // emit socket

        return new OrderAdminResource(
            $item
        );
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dealer $dealer, $id)
    {
        return response()->json([
            'is_deleted' => (bool)    $dealer->orders()->whereId($id)
                ->delete()
        ]);
    }
}
