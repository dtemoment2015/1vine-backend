<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\AdminResource;

use App\Models\Business\Shop;
use Illuminate\Http\Request;

class ShopUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($shopId = null)
    {
        return AdminResource::collection(
            Shop::whereId($shopId)
                ->firstOrFail()
                ->users()
                ->with('role')
                ->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $shopId)
    {
        Shop::whereId($shopId)->firstOrFail()->users()->syncWithoutDetaching([$request->user_id]);

        return response()->json(['data' => ['id' => $request->user_id]]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($shopId, $id)
    {
        return response()->json(['deleted' =>  Shop::whereId($shopId)->firstOrFail()->users()->detach([$id])]);
    }
}
