<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\PointAdminResource;
use App\Models\User;


class UserPointController extends Controller
{
    public function index(User $user)
    {
        $from_at = request('from_at', now()->startOfDay());
        $till_at = request('till_at', now()->endOfDay());

        return PointAdminResource::collection(
            $user->points()
                ->with(['shop.dealer', 'media'])
                ->orderByDesc('id')
                ->whereBetween(
                    'created_at',
                    [
                        $from_at,
                        $till_at
                    ]
                )
                ->get()
        )->additional(
            [
                'from_at' => $from_at,
                'till_at' => $till_at
            ]
        );
    }
}
