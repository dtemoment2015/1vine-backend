<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Client\ClientResource;
use App\Models\User\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  ClientResource::collection(
            Client::when(request('search'), function ($query) {
                $query->where('phone', 'like', '%' . request('search') . '%');
            })
                ->bonus()
                ->orderBy('id', 'DESC')
                ->paginate()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new ClientResource(
            Client::whereId($id)
                ->bonus()
                ->firstOrFail()
        );
    }
}
