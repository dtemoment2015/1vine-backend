<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Admin\AddPaymentRequest;
use App\Http\Resources\v1\Admin\PaymentResource;
use App\Jobs\v1\PayingJob;
use App\Models\Order\Order;
use Illuminate\Support\Facades\DB;

class OrderPaymentController extends Controller
{
    public function index(Order $order)
    {
        return PaymentResource::collection($order->payments()->with(['user'])->orderByDesc('paid_at')
            ->orderByDesc('id')
            ->paginate());
    }

    public function store(AddPaymentRequest $request, Order $order)
    {
        $user = $request->user();
        $payment = $order->payments()->create($request->all());
        $payment->user()->associate($user);
        $payment->save();

        $this->pay($order, $user);

        // PayingJob::dispatch($order)->onQueue('process');

        return new PaymentResource($payment);
    }

    public function destroy(Order $order, $id)
    {
        $deleted =  $order->payments()->whereId($id)->delete();
        $this->pay($order, request()->user());
        return response()->json(
            ['deleted' => $deleted]
        );
    }

    private function pay($order, $user)
    {
        $price = $order->purchases()->sum(DB::raw('quantity * price'));
        $lastDataPay = $order->payments()->orderByDesc('paid_at')->orderByDesc('id')->limit(1)->first();
        if ($order->payments()->sum('amount') >= $price) {
            $order->paid_at = $lastDataPay->paid_at;
            $order->is_paid = true;
            $order->save();
            $value = 0;
            if ($order->created_at->clone()->addDays(5) >= $order->paid_at ) {
                $value = $price * 0.1;
            }
            elseif ($order->created_at->clone()->addDays(15) >= $order->paid_at){
                $value = $price * 0.05;
            }
            if ($value && !$order->transaction()->exists()) {
                $transaction = $order->transaction()->create(['value' => $value]);
                $transaction->user()->associate($user);
                $transaction->activator()->associate($order->dealer);
                $transaction->save();
            }
        }
    }
}
