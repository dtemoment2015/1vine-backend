<?php

namespace App\Http\Controllers\v1\Admin;

use App\Events\v1\Dealer\OrderEvent;
use App\Exceptions\MyOrderException;
use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Dealer\CreateOrderRequest;
use App\Http\Resources\v1\Admin\OrderAdminResource;
use App\Http\Resources\v1\Dealer\OrderResource;
use App\Models\Business\Shop;
use App\Models\Storage\Status;
use App\Models\Warehouse\Option;
use App\Services\OneCSoapService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MyOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($dealerId = null)
    {
        return  OrderAdminResource::collection(

            request()->user()->orders()
                ->dateFilter()
                ->with(['dealer', 'customer.shop'])
                ->price()
                ->when($dealerId, function ($query) use ($dealerId) {
                    $query->whereHas('dealer', function ($query) use ($dealerId) {
                        $query->whereId($dealerId);
                    });
                })

                ->when(request('dealer_id'), function ($query) use ($dealerId) {
                    $query->whereHas('dealer', function ($query) use ($dealerId) {
                        $query->whereIn('id', explode('-', request('dealer_id')));
                    });
                })
                ->when(request('user_id'), function ($query) use ($dealerId) {
                    $query->whereHas('user', function ($query) use ($dealerId) {
                        $query->whereIn('id', explode('-', request('user_id')));
                    });
                })
                ->when(request('shop_id'), function ($query) {
                    $query->whereHas('customer', function ($query) {
                        $query->whereShopId(request('shop_id'));
                    });
                })
                ->when(request('status_order_id'), function ($query) {
                    $query->whereHas('status.status', function ($query) {
                        $query->whereIn('id', explode('-', request('status_order_id')));
                    });
                })
                ->when(request('role'), function ($query) {
                    $query->whereHas('dealer.role', function ($query) {
                        $query->whereCode(request('role'));
                    });
                })
                ->when(request('is_paid'), function ($query) {
                    $query->whereIsPaid((int) request('is_paid') - 1);
                })
                // ->price()
                ->when(request('order'), function ($query) {
                    $order = request('order');
                    $query
                        ->when($order == 'id', function ($query) {
                            $query->orderByDesc('orders.id');
                        })->when($order == '-id', function ($query) {
                            $query->orderBy('orders.id');
                        });
                })
                ->orderByDesc('orders.id')
                ->groupBy('orders.id')
                ->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOrderRequest $request, OneCSoapService $oneCSoapService)
    {

        try {
            $item =    DB::transaction(function () use ($request, $oneCSoapService) {
                $shop = Shop::whereId($request->shop_id)->first();

                if (!$shop->dealer) {
                    throw new MyOrderException(__('У магазина нет владельца. Заказ создать невозможно'), ['object' => [__('error.shop_error')]]);
                }

                $item =  $request->user()
                    ->orders()
                    ->create(
                        [
                            'code' => time(),
                        ]
                    );

                $item->customer()->create(
                    [
                        'name' => $shop->dealer->last_name . ' ' . $shop->dealer->first_name,
                        'phone' => $shop->dealer->phone,
                        'address' => $shop->address,
                        'location_id' => $shop->location_id,
                        'shop_id' => $shop->id,
                    ]
                );

                try {
                    if ($supervisorId = $request->user()->user_id) {
                        $item->attach_users()->syncWithoutDetaching([$supervisorId]);
                    }
                } catch (\Throwable $th) {
                    //throw $th;
                }

                $status = $item->statuses()->create([
                    'status_id' => Status::whereCode('accept')
                        ->firstOrFail()->id
                ]);

                $item->dealer_id = $shop->dealer->id;
                $item->order_status_id = $status->id;

                $item->save();

                try {
                    $purchases = Option::whereIn('id', array_keys($request->purchases))->get()->map(function ($option) use ($request) {
                        $qty =  (int) optional($request->purchases)[$option->id];
                        return [
                            'display_name' => optionName($option),
                            'price' => $option->price,
                            'sku' => (string) $option->sku,
                            'price' => $option->price,
                            'reward' => (float) $option->reward,
                            'option_id' => $option->id,
                            'price_old' => $option->price_old,
                            'quantity' => $qty,
                            'image' => optional(optional($option->images)[0])->file  ?  url('storage/options/x600/' . $option->images[0]->file) : "",
                        ];
                    });

                    $item->purchases()->createMany($purchases->where('quantity', '>', '0'));
                } catch (Exception $e) {
                }


                return $item;
            });
        } catch (\Throwable $th) {
            throw new MyOrderException(__('На данный момент заказ нельзя сформировать'), ['object' => [$th->getMessage()]]);
        }


        if (!$item) {
            throw new MyOrderException(__('На данный момент заказ нельзя сформировать'), ['object' => [__('Обновление системы')]]);
        }


        $data = new OrderAdminResource($item->load(['purchases', 'dealer']));

        // $oneCSoapService->send('Orders',  $data->jsonSerialize());

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new OrderAdminResource(
            request()->user()->orders()->whereId($id)
                ->with(['customer.shop', 'purchases'])
                ->price()
                ->reward()
                ->firstOrFail()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, OneCSoapService $oneCSoapService)
    {

        $item =  request()->user()->orders()->whereId($id)
            ->firstOrFail();

        $statusId = optional($request)->status_id;

        $item->update($request->all());

        if (($statusId != $item->status->status_id) || !$item->status) {
            $item->status =  $item
                ->status()
                ->create(
                    [
                        'status_id' => $statusId,
                        'user_id' => $request->user()->id
                    ]
                );


            if ($item->status->code == 'processed' && !$item->processed_at) {
                $item->processed_at = now();
                $send1C  = true;
                $item->save();
            }
        }


        $order = new OrderResource($item->load(['purchases']));

        event(
            new OrderEvent(
                $order
            )
        );


        $result = new OrderAdminResource(
            $item
        );

        if ($send1C) {
            $oneCSoapService->send('Orders',  $result->jsonSerialize());
        }

        return  $result;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json([
            'is_deleted' => (bool)  request()->user()->orders()->whereId($id)
                ->delete()
        ]);
    }
}
