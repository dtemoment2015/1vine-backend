<?php

namespace App\Http\Controllers\v1\Admin;

use App\Events\v1\User\OrderEvent;
use App\Http\Controllers\Controller;

use App\Http\Resources\v1\Admin\OrderAdminResource;


use App\Models\User;
use Illuminate\Http\Request;

class UserOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $User)
    {
        return  OrderAdminResource::collection(

            $User->orders()
                ->price()
                ->with(['status', 'dealer'])
                ->orderByDesc('orders.id')
                ->groupBy('orders.id')
                ->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $User)
    {
        $item =   $User->orders()->create([
            'code' => time(),
            'User_id' => $request->User_id
        ]);

        $User = User::whereId($request->User_id)->firstOrFail();

        $shop = $User->shops()->whereId($request->shop_id)->firstOrFail();

        $item->customer()->create(
            [
                'name' => $User->last_name . ' ' . $User->first_name,
                'phone' => $User->phone,
                'address' => $shop->address,
                'location_id' => $shop->location_id,
                'shop_id' => $shop->id,
            ]
        );

        $item->status()->create(['status_id' => $request->status_id]);

        return new OrderAdminResource(
            $item
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $User, $id)
    {
        return new OrderAdminResource(
            $User->orders()->whereId($id)
                ->with('customer.shop')
                ->price()
                ->reward()
                ->firstOrFail()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $User, $id)
    {

        $item =    $User->orders()->whereId($id)
            ->firstOrFail();

        $statusId = optional($request)->status_id;

        $item->update($request->all());

        if (($statusId != optional($item->status)->status_id) || !$item->status) {
          $status =  $item->statuses()
                ->create(
                    [
                        'status_id' => $statusId,
                        'user_id' => $request->user()->id
                    ]
                );
                
              $item->order_status_id = $status->id;  
              $item->save();

        }

        $order =   new OrderAdminResource($item->load(['purchases']));

    

        return new OrderAdminResource(
            $item
        );
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $User, $id)
    {
        return response()->json([
            'is_deleted' => (bool)    $User->orders()->whereId($id)
                ->delete()
        ]);
    }
}
