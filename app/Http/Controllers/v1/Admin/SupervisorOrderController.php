<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\OrderAdminResource;
use Illuminate\Http\Request;

class SupervisorOrderController extends Controller
{
    public function index()
    {
        return OrderAdminResource::collection(request()
            ->user()
            ->attach_orders()
            ->with(['dealer','user', 'status','attach_users'])
            ->dateFilter()
            ->when(request('status_order_id'), function ($query) {
                $query->whereHas('status.status', function ($query) {
                    $query->whereIn('id', explode('-', request('status_order_id')));
                });
            })
            ->when(request('dealer_id'), function ($query) {
                $query->whereHas('dealer', function ($query) {
                    $query->whereIn('id', explode('-', request('dealer_id')));
                });
            })
            ->when(request('user_id'), function ($query) {
                $query->whereHas('user', function ($query) {
                    $query->whereIn('id', explode('-', request('user_id')));
                });
            })
            ->when(request('status_order_id'), function ($query) {
                $query->whereHas('status.status', function ($query) {
                    $query->whereIn('id', explode('-', request('status_order_id')));
                });
            })
            ->when(request('is_paid'), function ($query) {
                $query->whereIsPaid((int) request('is_paid') - 1);
            })
            ->orderByDesc('orders.id')
            ->groupBy('orders.id')
            ->paginate());
    }

}
