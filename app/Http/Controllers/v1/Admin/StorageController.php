<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\LocationAdminCollection;
use App\Http\Resources\v1\LocationResource;
use App\Models\Cloud\Location;
use App\Models\Text;
use Illuminate\Support\Facades\Cache;

class StorageController extends Controller
{
    public function index()
    {
        return response()->json(
            ['data' =>   [
                'languages' => collect(config('translatable.localizations'))->pluck('title', 'key'),
                'locations' => new LocationAdminCollection(
                    Cache::rememberForever('location-admin' . request('_localization'), function () {
                        return   LocationResource::collection(Location::whereParentId('>', 0)->with('childs')->get());
                    })
                ),
                'text' =>      Cache::rememberForever('text-' . request('_localization'),  function () {
                    return  Text::get()->mapToGroups(function ($item) {
                        return [$item->group => [$item->key => $item->title]];
                    })
                        ->map(function ($i) {
                            return $i->collapse();
                        })
                        ->toArray();
                }),
            ]]
        );
    }
}
