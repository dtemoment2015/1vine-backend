<?php

namespace App\Http\Controllers\v1\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Admin\LoginAdminRequest;
use App\Http\Resources\v1\Admin\UserResource;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class IndexController extends Controller
{
    public function login(LoginAdminRequest $request)
    {
        $user = User::wherePhone($request->phone)
            ->limit(1)
            ->firstOrFail();
            
        if (Hash::check(
            $request->password,
            $user->password
        )) {
            $token = $user->createToken('ADMIN');
            return response()->json(new UserResource($user))->withHeaders([
                'X-Access-Token' => optional($token)->accessToken,
                'X-Expires-At' => optional(optional($token)->token)['expires_at']
            ]);
        } else {
            return response()->json(
                [
                    'message' => __('error.form_validate'),
                    'errors' => [
                        'phone' => [__('auth.access')]
                    ]
                ],
                422
            );
        }
    }
}
