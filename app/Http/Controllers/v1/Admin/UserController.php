<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\AdminResource;
use App\Jobs\v1\SmsJob;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  AdminResource::collection(
            User::with(['role', 'point'])
                ->when(request('role_code'), function ($query) {
                    $query->whereHas('role', function ($query) {
                        $query->whereIn('code', explode('-', request('role_code')));
                    });
                })
                ->when(request('with_point'), function ($query) {
                    $query->whereHas('point');
                })
                ->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return new AdminResource(
            User::create($request->all())
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new AdminResource(
            User::with(['role', 'user'])->whereId($id)
                ->firstOrFail()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item =   User::whereId($id)
            ->firstOrFail();

        $item->update($request->all());

        if (optional($request)->password) {
            SmsJob::dispatch($item, __(
                'dealer.system.password',
                ['password' => $request->password]
            ))->onQueue('sms');
        }

        return new AdminResource(
            $item
        );
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return new AdminResource(
            [
                'is_deleted' => (bool)  User::whereId($id)
                    ->delete()
            ]
        );
    }
}
