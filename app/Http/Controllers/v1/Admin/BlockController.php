<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\BlockResource;
use App\Models\Storage\Block;


class BlockController extends Controller
{
    public function index()
    {
        return BlockResource::collection(
            Block::paginate()
        );
    }
}
