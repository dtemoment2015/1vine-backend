<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Models\Storage\Status;
use App\Models\User\Dealer;
use App\Models\Order\Order;
use App\Models\User\Transaction;
use App\Models\Warehouse\Serial;
use App\Models\Warehouse\Sticker;

class StatController extends Controller
{
    public function index()
    {
        return response()->json(
            ['data' => [
                [
                    'key' => 'orders',
                    'title' => 'Всего заказов',
                    'icon' => 'ti-shopping-cart-full',
                    'value' => Order::count()
                ],
                [
                    'key' => 'dealers',
                    'title' => 'Всего дилеров',
                    'icon' => 'ti-id-badge',
                    'value' => Dealer::count()
                ],
                [
                    'key' => 'serials',
                    'title' => 'Всего с/н',
                    'icon' => 'ti-package',
                    'value' => Serial::count()
                ],
                [
                    'key' => 'stickers',
                    'title' => 'Всего наклеек',
                    'icon' => 'ti-layout-cta-right',
                    'value' => Sticker::count()
                ],
                [
                    'key' => 'sale',
                    'title' => 'Бонусы за продажи',
                    'icon' => 'ti-money',
                    'value' => Transaction::whereHasMorph('item', [Serial::class])->where('value', '>', 0)->sum('value')
                ],
                [
                    'key' => 'add',
                    'title' => 'Бонусы за начисление',
                    'icon' => 'ti-stats-up',
                    'value' => Transaction::whereHasMorph('item', [Status::class])->where('value', '>', 0)->sum('value')
                ],
                [
                    'key' => 'withdraw',
                    'title' => 'Вывод средств',
                    'icon' => 'ti-stats-down',
                    'value' => Transaction::whereHasMorph('item', [Status::class])->where('value', '<', 0)->sum('value')
                ],
                [
                    'key' => 'transactions',
                    'title' => 'Совокупность бонусов',
                    'icon' => 'ti-briefcase',
                    'value' => Transaction::sum('value')
                ],
            ],]
        );
    }
}
