<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Admin\DealerUpdateRequest;
use App\Http\Resources\v1\Admin\DealerAdminResource;
use App\Jobs\v1\SmsJob;
use App\Models\User\Dealer;
use Illuminate\Http\Request;

class MyDealerController extends Controller
{
    public function index()
    {
        return  DealerAdminResource::collection(
            request()
                ->user()
                ->dealers()
                ->when(request('search'), function ($query) {
                    $query->search(request('search'))
                        ->orderBy('relevance', 'DESC');
                })
                ->when(request('role_id'), function ($query) {
                    $query->whereHas('role', function ($query) {
                        $query->whereId(request('role_id'));
                    });
                })
                ->when(request('role'), function ($query) {
                    $query->whereHas('role', function ($query) {
                        $query->whereCode(request('role'));
                    });
                })
                ->when(request('shop_id'), function ($query) {
                    $query->whereHas('shops', function ($query) {
                        $query->whereIn('id', explode('-', request('shop_id')));
                    });
                })
                ->when(request('user_id'), function ($query) {
                    $query->whereHas('users', function ($query) {
                        $query->whereIn('id', explode('-', request('user_id')));
                    });
                })
                ->when(request('is_active'), function ($query) {
                    $query->whereIsActive((int) request('is_active') - 1);
                })
                ->when(request('role_code'), function ($query) {
                    $query->whereHas('role', function ($query) {
                        $query->whereCode(request('role_code'));
                    });
                })
                //    ->bonus()
                ->orderBy('id', 'DESC')
                ->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DealerUpdateRequest $request)
    {

        $item =  Dealer::create($request->all());
        $item->is_active = 1;

        $item->save();

        $item->users()->syncWithoutDetaching([$request->user()->id]);

        $item->dealer_temp()->delete();

        return new DealerAdminResource(
            $item
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DealerUpdateRequest $request, $id)
    {

        $item =   Dealer::whereId($id)
            ->firstOrFail();

        if ($item->is_active && !$request->is_active) {
            $temp = $item->dealer_temp()->firstOrCreate();
            $temp->code = setCode();
            $temp->address = '';
            $temp->save();
        }

        $item->update($request->all());

        if (optional($request)->password) {
            SmsJob::dispatch($item, __(
                'dealer.system.password',
                ['password' => $request->password]
            ))->onQueue('sms');
        }

        // $item->shops()->sync(
        //     (array) optional($request)->shops
        // );

        $item->users()->sync(
            (array) optional($request)->users
        );


        if ($item->is_active) {
            $temp = $item->dealer_temp()->delete();
        }

        return new DealerAdminResource(
            $item->load('dealer_temp')
        );
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return new DealerAdminResource(
            [
                'is_deleted' => (bool)  Dealer::whereId($id)
                    ->delete()
            ]
        );
    }
}
