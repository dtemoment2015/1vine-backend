<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Admin\Order\PurchaseRequest;
use App\Http\Resources\v1\Admin\PurchaseAdminResource;
use App\Http\Resources\v1\Dealer\OptionResource;
use App\Models\Order\Order;
use App\Models\Order\OrderPurchase;
use App\Models\Warehouse\Option;
use Illuminate\Http\Request;

class OrderPurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($orderId = null)
    {
        return  PurchaseAdminResource::collection(
            Order::whereId($orderId)->firstOrFail()->purchases()->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store($orderId = null, PurchaseRequest $request)
    {

        $option = new OptionResource(Option::whereId($request->option_id)->firstOrFail());

        return new  PurchaseAdminResource(
            Order::whereId($orderId)
                ->firstOrFail()
                ->purchases()->updateOrCreate(
                    [
                        'option_id' => $option->id
                    ],
                    [
                        'quantity' => $request->quantity,
                        'display_name' => optionName($option),
                        'price' => $option->price,
                        'reward' => $option->reward,
                        'option_id' => $option->id,
                    ]
                )
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new  PurchaseAdminResource(
            OrderPurchase::whereId($id)
                ->firstOrFail()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PurchaseRequest $request, $id)
    {
        $item =   OrderPurchase::whereId($id)
            ->firstOrFail();

        $item->update($request->all());

        return new  PurchaseAdminResource(
            $item
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($orderId = null, $id)
    {
        return response()->json(
            [
                'is_deleted' => (bool) Order::whereId($orderId)
                    ->firstOrFail()
                    ->purchases()
                    ->whereId($id)
                    ->delete()
            ]
        );
    }
}
