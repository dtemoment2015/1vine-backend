<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\RatingResource;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class RatingController extends Controller
{
    public function index(User $user)
    {
        return RatingResource::collection(
            $user
                ->with(['role'])
                ->withCount('dealers as dealers_count')
                ->withCount('shops as shops_count')
                ->withCount(['orders as orders_count' => function ($query) {
                    $query
                    ->dateFilter();
                }])
                ->withCount(['orders as orders_done_count' => function ($query) {
                    $query->whereHas('status.status', function ($query) {
                        $query->whereCode('done');
                    })
                    ->dateFilter();
                }])
                ->withCount(['purchases as purchases_total' => function ($query) {
                    $query->select(DB::raw('SUM(order_purchases.quantity)'))
                        ->whereHas('order', function ($query) {
                            $query->dateFilter()->whereHas('status.status', function($query){
                                $query->whereCode('done');
                            });
                        });
                }])
                ->withCount(['purchases as order_prices_total' => function ($query) {
                    $query->select(DB::raw('SUM(order_purchases.price * order_purchases.quantity)'))
                        ->whereHas('order', function ($query) {
                            $query->dateFilter()->whereHas('status.status', function($query){
                                $query->whereCode('done');
                            });
                        });
                }])
                ->when(request('shop_id'), function ($query) {
                    $query->whereHas('shops', function ($query){
                        $query->whereIn('id', explode('-', request('shop_id')));
                    });
                })
                ->when(request('dealer_id'), function ($query) {
                    $query->whereHas('dealers', function ($query){
                        $query->whereIn('id', explode('-', request('dealer_id')));
                    });
                })
                ->orderByDesc('order_prices_total')
                ->orderByDesc('purchases_total')
                ->orderByDesc('orders_done_count')
                ->orderByDesc('orders_count')
                ->orderByDesc('shops_count')
                ->orderByDesc('dealers_count')
                ->orderByDesc('id')
                ->paginate()
        );
    }

    public function show($id, User $user)
    {
        return new RatingResource($user->whereId($id)
        ->with(['role'])
        ->withCount('dealers as dealers_count')
        ->withCount('shops as shops_count')
        ->withCount(['orders as orders_count'])
        ->withCount(['orders as orders_done_count' => function ($query) {
            $query->whereHas('status.status', function ($query) {
                $query->whereCode('done');
            });
        }])
        // ->withSum('purchases as purchases_total', 'quantity', function ($query) {
        //     $query->whereHas('order', function ($query) {
        //         $query->dateFilter()
        //         ->whereHas('status', function($query){
        //             $query->whereCode('done');
        //         });
        //     });
        // })
        ->withCount(['purchases as purchases_total' => function ($query) {
            $query->select(DB::raw('SUM(order_purchases.quantity)'))
                ->whereHas('order', function ($query) {
                    $query->whereHas('status.status', function($query){
                        $query->whereCode('done');
                    });
                });
        }])
        ->withCount(['purchases as order_prices_total' => function ($query) {
            $query->select(DB::raw('SUM(order_purchases.price * order_purchases.quantity)'))
                ->whereHas('order', function ($query) {
                    $query->whereHas('status.status', function($query){
                        $query->whereCode('done');
                    });
                });
        }])
            ->firstOrFail());
    }
}
