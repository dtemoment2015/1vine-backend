<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Client\TransactionResouce;
use App\Models\User\Client;
use Illuminate\Http\Request;

class ClientTransactionController extends Controller
{
    public function index($clientId = null)
    {
        return  TransactionResouce::collection(
            Client::whereId($clientId)
                ->firstOrFail()
                ->transactions()
                ->orderBy('id', 'DESC')
                ->paginate()
        );
    }
}
