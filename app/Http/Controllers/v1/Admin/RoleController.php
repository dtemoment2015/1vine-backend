<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\RoleAdminResource;
use App\Models\Business\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return RoleAdminResource::collection(
            Role::paginate()
        );
    }

    public function show(Role $role)
    {
        return new RoleAdminResource(
            $role->load('navigations')
        );
    }

    public function update(Request $request, Role $role)
    {
        $role->update($request->all());
        $role->navigations()->sync((array) optional($request)->navigations);
        return new RoleAdminResource(
            $role
        );
    }

    public function store(Request $request, Role $role)
    {

        $item = $role->create($request->all());

        $item->navigations()->sync((array) optional($request)->navigations);

        return new RoleAdminResource(
            $item
        );
    }

    public function destroy($id)
    {
        return response()->json(['deleted' =>  Role::whereId($id)->delete()]);
    }
}
