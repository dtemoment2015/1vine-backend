<?php

namespace App\Http\Controllers\v1\Admin\Profile;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\UserResource;

class IndexController extends Controller
{
    public function index()
    {
        return response()->json(
            new UserResource(request()->user())
        );
    }
}
