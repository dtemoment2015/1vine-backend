<?php

namespace App\Http\Controllers\v1\Admin;

use App\Events\UserPointEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Admin\AddPointRequest;
use App\Http\Resources\v1\Admin\PointResource;

class PointController extends Controller
{

    public function index()
    {
        return PointResource::collection(
            request()->user()->points()->with(['shop','media'])->orderByDesc('id')->paginate()
        );
    }

    public function store(AddPointRequest $request)
    {
        $user = $request->user();

        $point =  $user->points()->create($request->all())->load('shop');

        if (($images = (array) optional($request)->media) &&
            is_array($images) &&
            count($images)
        ) {
            foreach ($images as $image) {
                $point->addMedia($image)->toMediaCollection('points');
            }
        }

        event(
            new UserPointEvent(
                $user->load('point.media')
            )
        );
        
        return new PointResource(
            $point->load('media')
        );
    }
}
