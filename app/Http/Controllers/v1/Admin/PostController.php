<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\PostAdminResource;
use App\Library\ImageUpload;
use App\Models\Storage\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  PostAdminResource::collection(
            Post::orderBy('id', 'DESC')
                ->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();
        //FIX
        foreach (config('translatable.locales') as $lang) {
            if (!$input['image:' . $lang]  =  $this->multiImage($input, $lang, 'image', 'posts')) {
                unset($input['image:' . $lang]);
            }
        }

        return new PostAdminResource(
            Post::create($input)
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new PostAdminResource(
            Post::whereId($id)
                ->with('contents')
                ->firstOrFail()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item =   Post::whereId($id)
            ->firstOrFail();

        $input = $request->all();

        if (
            optional($request)->is_content
            && is_array(optional($request)->contents)
        ) {
            $item->contents =  $this->contents($request, $item);
        } else {
            //FIX
            foreach (config('translatable.locales') as $lang) {
                if (!$input['image:' . $lang]  =  $this->multiImage($input, $lang, 'image', 'posts')) {
                    unset($input['image:' . $lang]);
                }
            }
            $item->update($input);
        }

        return new PostAdminResource(
            $item->load('contents')
        );
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json( [
            'is_deleted' => (bool)  Post::whereId($id)
                ->delete()
        ]);
    

    }


    private function multiImage($input = [], $lang = null, $key = null, $folder = 'apps')
    {
        if ($lang  && $key && optional($input)[$key . ':' . $lang] && !filter_var($input[$key . ':' . $lang], FILTER_VALIDATE_URL)) {
            return ImageUpload::load(
                $input[$key . ':' . $lang],
                '',
                $folder,
                [400]
            );
        }

        return null;
    }

    private function contents($request = null, $item = null)
    {
        if ($request && $item) {

            $contents = collect($request->contents);

            $contents->where('is_remove', false)->map(function ($i, $k) use ($item) {
                if ($i) {

                    if (optional($i)['id']) {
                        optional($item->contents()
                            ->whereId($i['id'])->first())
                            ->update($i);
                    } else {
                        $i['position'] = time() + $k;

                        $i['id']  = optional($item
                            ->contents()
                            ->create(
                                $i
                            ))->id;
                    }
                }
                return $i;
            });

            $contents->where('is_remove', true)->map(function ($i) use ($item) {
                $item->contents()
                    ->whereId(optional($i)['id'])
                    ->delete();
                return $i;
            });
        }

        return $item->contents()->get();
    }
}
