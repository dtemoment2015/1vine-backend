<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\ShopAdminResource;
use App\Models\Business\Shop;

use Illuminate\Http\Request;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  ShopAdminResource::collection(
            Shop::when(request('search'), function ($query) {
                $query->search(request('search'))
                    ->orderBy('relevance', 'DESC');
            })
                ->when(request('dealer_id'), function ($query) {
                    $query->whereHas('dealer', function ($query) {
                        $query->whereId(request('dealer_id'));
                    });
                })
                ->when(request('user_id'), function ($query) {
                    $query->whereHas('users', function ($query) {
                        $query->whereId(request('user_id'));
                    });
                })
                ->when(request('order'), function ($query) {
                    $order = request('order');
                    $query
                        ->when($order == 'id', function ($query) {
                            $query->orderByDesc('id');
                        })->when($order == '-id', function ($query) {
                            $query->orderBy('id');
                        });
                })
                ->with(['dealer', 'location.parent', 'business', 'media'])
                ->orderByDesc('id')
                ->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item =  Shop::create($request->all());

        if (($images = optional($request)->images) &&
            is_array($images) &&
            count($images)
        ) {
            foreach ($images as $image) {
                $item->addMedia($image)->toMediaCollection('shops');
            }
        }


        return new ShopAdminResource(
            $item
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new ShopAdminResource(
            Shop::whereId($id)
                ->with('media', 'dealer')
                ->firstOrFail()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $item  =   Shop::whereId($id)
            ->firstOrFail();

        if (($images = optional($request)->images) &&
            is_array($images) &&
            count($images)
        ) {
            foreach ($images as $image) {
                $item->addMedia($image)->toMediaCollection('shops');
            }
        }

        $item->update($request->all());


        return new ShopAdminResource(
            $item->load('media')
        );
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        return response()->json([
            'is_deleted' => (bool)  Shop::whereId($id)
                ->delete()
        ]);
    }
}
