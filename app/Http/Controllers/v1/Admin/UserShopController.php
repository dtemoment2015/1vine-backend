<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Dealer\ShopAddRequest;
use App\Http\Resources\v1\Admin\ShopAdminResource;
use App\Models\User;
use Illuminate\Http\Request;

class UserShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        return  ShopAdminResource::collection(
            $user->shops()
                ->with('dealer')
                ->orderByDesc('id')
                ->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ShopAddRequest $request, User $user)
    {
        $item =  $user->shops()->create($request->all());

        if (($images = optional($request)->images) &&
            is_array($images) &&
            count($images)
        ) {
            foreach ($images as $image) {
                $item->addMedia($image)->toMediaCollection('shops');
            }
        }

        return new ShopAdminResource(
            $item
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, $id)
    {
        // return new ShopAdminResource(
        //     User::whereId($UserId)->firstOrFail()->shops()->whereId($id)
        //         ->with('media', 'User')
        //         ->firstOrFail()
        // );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user, $id)
    {

     
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, $id)
    {
        return response()->json(
            [
                'is_deleted' => (bool) $user
                    ->shops()
                    ->whereId($id)
                    ->delete()
            ]
        );
    }
}
