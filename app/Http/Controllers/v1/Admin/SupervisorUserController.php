<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Admin\AddUserRequest;
use App\Http\Resources\v1\Admin\AdminResource;
use App\Http\Resources\v1\Admin\RatingResource;

use App\Models\Business\Role;

use Illuminate\Support\Facades\DB;

class SupervisorUserController extends Controller
{
    public function index()
    {
        return RatingResource::collection(request()->user()->users()
            ->with(['role'])
            ->withCount('dealers as dealers_count')
            ->withCount('shops as shops_count')
            ->withCount(['orders as orders_count' => function ($query) {
                $query
                    ->dateFilter();
            }])
            ->withCount(['orders as orders_done_count' => function ($query) {
                $query->whereHas('status.status', function ($query) {
                    $query->whereCode('done');
                })
                    ->dateFilter();
            }])
            ->withCount(['purchases as purchases_total' => function ($query) {
                $query->select(DB::raw('SUM(order_purchases.quantity)'))
                    ->whereHas('order', function ($query) {
                        $query->dateFilter()->whereHas('status.status', function ($query) {
                            $query->whereCode('done');
                        });
                    });
            }])
            ->withCount(['purchases as order_prices_total' => function ($query) {
                $query->select(DB::raw('SUM(order_purchases.price * order_purchases.quantity)'))
                    ->whereHas('order', function ($query) {
                        $query->dateFilter()->whereHas('status.status', function ($query) {
                            $query->whereCode('done');
                        });
                    });
            }])
            ->when(request('shop_id'), function ($query) {
                $query->whereHas('shops', function ($query) {
                    $query->whereIn('id', explode('-', request('shop_id')));
                });
            })
            ->when(request('dealer_id'), function ($query) {
                $query->whereHas('dealers', function ($query) {
                    $query->whereIn('id', explode('-', request('dealer_id')));
                });
            })
            ->orderByDesc('order_prices_total')
            ->orderByDesc('purchases_total')
            ->orderByDesc('orders_done_count')
            ->orderByDesc('orders_count')
            ->orderByDesc('shops_count')
            ->orderByDesc('dealers_count')
            ->orderByDesc('id')

            ->paginate());
    }
}
