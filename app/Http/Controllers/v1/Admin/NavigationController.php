<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\NavigationAdminResource;
use App\Models\Storage\Navigation;
use Illuminate\Http\Request;

class NavigationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return NavigationAdminResource::collection(
            Navigation::with('roles')->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $navigation = Navigation::create($request->all());

        $navigation->roles()->sync(
            (array) optional($request)->roles
        );
        $navigation->blocks()->sync(
            (array) optional($request)->blocks
        );

        return new NavigationAdminResource(
            $navigation 
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new NavigationAdminResource(
            Navigation::whereId($id)
            ->with(['blocks','roles'])
                ->firstOrFail()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $item =   Navigation::whereId($id)
            ->firstOrFail();

        $item->update($request->all());


        $item->roles()->sync(
            (array) optional($request)->roles
        );
        $item->blocks()->sync(
            (array) optional($request)->blocks
        );

        return new NavigationAdminResource(
            $item
        );
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return new NavigationAdminResource(
            [
                'is_deleted' => (bool)  Navigation::whereId($id)
                    ->delete()
            ]
        );
    }
}
