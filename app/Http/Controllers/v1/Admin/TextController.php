<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\TextAdminResource;
use App\Models\Text;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class TextController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  TextAdminResource::collection(
            Text::orderBy('group')
                ->orderByTranslation('title')
                ->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return new TextAdminResource(
            Text::create($request->all())
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new TextAdminResource(
            Text::whereId($id)
                ->firstOrFail()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        foreach (config('translatable.locales') as $lang) {
            Cache::forget('text-' . $lang);
        }

        $item =   Text::whereId($id)
            ->firstOrFail();

        $item->update($request->all());

        return new TextAdminResource(
            $item
        );
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return new TextAdminResource(
            [
                'is_deleted' => (bool)  Text::whereId($id)
                    ->delete()
            ]
        );
    }
}
