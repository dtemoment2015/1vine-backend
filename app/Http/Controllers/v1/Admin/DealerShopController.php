<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Dealer\ShopAddRequest;
use App\Http\Resources\v1\Admin\ShopAdminResource;
use App\Models\User\Dealer;
use Illuminate\Http\Request;

class DealerShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($dealerId)
    {
        return  ShopAdminResource::collection(
            Dealer::whereId($dealerId)->firstOrFail()->shops()->when(request('dealer_id'), function ($query) {
                $query->whereHas('dealers', function ($query) {
                    $query->whereId(request('dealer_id'));
                });
            })
                ->when(request('order'), function ($query) {
                    $order = request('order');
                    $query
                        ->when($order == 'id', function ($query) {
                            $query->orderByDesc('id');
                        })->when($order == '-id', function ($query) {
                            $query->orderBy('id');
                        });
                })
                ->with('dealer')
                ->orderByDesc('id')
                ->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ShopAddRequest $request, $dealerId)
    {
        $item =  Dealer::whereId($dealerId)->firstOrFail()->shops()->create($request->all());

        if (($images = optional($request)->images) &&
            is_array($images) &&
            count($images)
        ) {
            foreach ($images as $image) {
                $item->addMedia($image)->toMediaCollection('shops');
            }
        }

        return new ShopAdminResource(
            $item
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($dealerId, $id)
    {
        // return new ShopAdminResource(
        //     Dealer::whereId($dealerId)->firstOrFail()->shops()->whereId($id)
        //         ->with('media', 'dealer')
        //         ->firstOrFail()
        // );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $dealerId, $id)
    {

        // $item  =   Dealer::whereId($dealerId)->firstOrFail()->shops()->whereId($id)
        //     ->firstOrFail();

        // if (($images = optional($request)->images) &&
        //     is_array($images) &&
        //     count($images)
        // ) {
        //     foreach ($images as $image) {
        //         $item->addMedia($image)->toMediaCollection('shops');
        //     }
        // }

        // $item->update($request->all());


        // return new ShopAdminResource(
        //     $item->load('media')
        // );
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($dealerId, $id)
    {
        return response()->json(
            [
                'is_deleted' => (bool)  Dealer::whereId($dealerId)
                    ->firstOrFail()
                    ->shops()
                    ->whereId($id)
                    ->delete()
            ]
        );
    }
}
