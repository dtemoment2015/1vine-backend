<?php

namespace App\Http\Controllers\v1\Exchange;

use App\Exports\OneCExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Admin\ExchangeOneCRequest;
use App\Http\Resources\v1\Admin\ImportResource;
use App\Jobs\v1\ImportOptionOneCJob;
use App\Models\Import;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class OneCController extends Controller
{

    public function store(ExchangeOneCRequest $request)
    {

        $import = Import::create();
        $tempFile = $import->id . '.xlsx';
        $data = new OneCExport($request->data);
        foreach ($request->data as $option) {
            ImportOptionOneCJob::dispatch((object)  $option)->onQueue('import');
        }
        Excel::store($data, $tempFile);
        $import->addMedia(Storage::disk('local')->path($tempFile))->toMediaCollection('import_file');
        return new ImportResource($import);
    }
}
