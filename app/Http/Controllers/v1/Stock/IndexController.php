<?php

namespace App\Http\Controllers\v1\Stock;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Dealer\OptionCollection;
use App\Http\Resources\v1\Dealer\OptionResource;
use App\Models\Warehouse\Option;

class IndexController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new OptionCollection(
            Option::filter()
                ->dealer()
                ->stock()
                ->orderBy('price')
                // ->orderBy('brand_id')
                ->paginate((int) (request('per_page', request('allGoods') ? 9999999999 : 50)))
        );
    }

    public function test()
    {
        return new OptionCollection(
            Option::filter()
                ->dealer()
                ->stock()
                ->with(['brand'])
                ->orderBy('price')
                // ->orderBy('brand_id')
                ->paginate()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Option $option)
    {
        return response()->json(
            new OptionResource(
                $option
                    ->with('contents')
                    ->feature()
                    ->dealer()
                    ->whereId($id)
                    ->limit(1)
                    ->firstOrFail()
            )
        );
    }
}
