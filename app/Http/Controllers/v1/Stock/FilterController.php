<?php

namespace App\Http\Controllers\v1\Stock;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Dealer\BrandResource;
use App\Models\Warehouse\Brand;
use Illuminate\Http\Request;

class FilterController extends Controller
{
    public function index()
    {
        return response()->json(
            [
                'brands' => BrandResource::collection(
                    Brand::whereHas('products', function ($query) {
                        $query->stock();
                    })
                        ->orderBy('name')
                        ->get()
                )
            ]
        );
    }
}
