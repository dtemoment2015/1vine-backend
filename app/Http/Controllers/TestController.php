<?php

namespace App\Http\Controllers;

use App\Models\Storage\Navigation;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index()
    {

        $a = [
            [
                'icon' => 'ti-basket',
                'title' => 'Мои заказы',
                'path' => 'my-orders',
                'component' => 'MyOrders',
            ],
            [
                'icon' => 'ti-dashboard',
                'title' => 'Экраны приложений',
                'path' => 'navigations',
                'component' => 'Navigations',
            ],
            [
                'icon' => 'ti-dashboard',
                'title' => 'Главная',
                'path' => 'dashboard',
                'component' => 'Dashboard',
            ],
            [
                'icon' => 'ti-user',
                'title' => 'Пользователи',
                'path' => 'dealers',
                'component' => 'Dealers',
            ],
            [
                'icon' => 'ti-bag',
                'title' => 'Заказы',
                'path' => 'orders',
                'component' => 'Orders',
            ],
            [
                'icon' => 'ti-list-ol',
                'title' => 'Транзакции',
                'path' => 'transactions',
                'component' => 'Transactions',
            ],
            [
                'icon' => 'ti-home',
                'title' => 'Магазины',
                'path' => 'shops',
                'component' => 'Shops',
            ],
            [
                'icon' => 'ti-view-grid',
                'title' => 'Склад',
                'path' => 'warehouse',
                'component' => 'Warehouse',
            ],
            [
                'icon' => 'ti-key',
                'title' => 'Администраторы',
                'path' => 'users',
                'component' => 'Users',
            ],
            [
                'icon' => 'ti-calendar',
                'title' => 'Новости',
                'path' => 'posts',
                'component' => 'Posts',
            ],
            [
                'icon' => 'ti-text',
                'title' => 'Системные слова',
                'path' => 'texts',
                'component' => 'Texts',
            ],
            [
                'icon' => 'ti-user',
                'title' => 'Роли пользователей',
                'path' => 'roles',
                'component' => 'Roles',
            ],
            [
                'icon' => 'ti-power-off',
                'title' => 'Выход',
                'path' => 'quit',
                'component' => 'Quit',
            ],

            [
                'icon' => 'ti-basket',
                'title' => 'Мои торговые точки',
                'path' => 'my-shops',
                'component' => 'MyShops',
            ],
        ];

        foreach ($a as $b) {
            Navigation::create(
                [
                    'icon' => $b['icon'],
                    'title:ru' => $b['title'],
                    'component' => $b['component'],
                    'code' => $b['path'],
                ]
            );
        }
    }
}
