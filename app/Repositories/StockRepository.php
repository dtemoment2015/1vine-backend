<?php

namespace App\Repositories;

use App\Models\Warehouse\Option;
use App\Repositories\Interfaces\StockRepositoryInterface;

class StockRepository implements StockRepositoryInterface
{
   public function list($request = [])
   {
      return Option::paginate();
   }
}
