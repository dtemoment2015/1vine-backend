<?php


namespace App\Repositories\Interfaces;

interface StockRepositoryInterface
{
   public function list($request);
}
