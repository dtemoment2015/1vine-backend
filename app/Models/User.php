<?php

namespace App\Models;

use App\Models\Business\Role;
use App\Models\Business\Shop;
use App\Models\Order\Order;
use App\Models\Order\OrderPurchase;
use App\Models\User\Dealer;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

use Nicolaslopezj\Searchable\SearchableTrait;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SearchableTrait;


    protected $searchable = [
        'columns' => [
            'users.name' => 4,
            'users.phone' => 5,
        ]
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'phone',
        'password',
        'role_id',
        'user_id',
        'code',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function user()
    {
        return $this->belongsTo(self::class);
    }

    public function users()
    {
        return $this->hasMany(self::class);
    }

    public function purchases()
    {
        return $this->hasManyThrough(OrderPurchase::class, Order::class);
    }

    public function setPasswordAttribute($val)
    {
        $this->attributes['password'] = bcrypt($val);
    }


    public function shops()
    {
        return $this->belongsToMany(Shop::class);
    }


    public function dealers()
    {
        return $this->belongsToMany(Dealer::class);
    }

    //for supervisor
    public function attach_orders()
    {
        return $this->belongsToMany(Order::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function points()
    {
        return $this->hasMany(Point::class);
    }
    public function point()
    {
        return $this->hasOne(Point::class)
            ->whereNotNull('lon')
            ->whereNotNull('lat')
            ->orderByDesc('id');
    }

    public function role()
    {
        return $this->belongsTo(
            Role::class
        );
    }
}
