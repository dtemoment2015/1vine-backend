<?php

namespace App\Models\Storage;

use App\Models\Warehouse\Option;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Illuminate\Database\Eloquent\Relations\Relation;

Relation::enforceMorphMap([
    'option' => Option::class,
    'post' => Post::class,
]);

class Content extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    public $translatedAttributes = [
        'value',
    ];

    protected $fillable = ['id', 'position'];

    public function item()
    {
        return $this->morphTo();
    }
}
