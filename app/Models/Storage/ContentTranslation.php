<?php

namespace App\Models\Storage;

use App\Models\Helpers\MultiPrimaryKeyTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContentTranslation extends Model
{
    use HasFactory, MultiPrimaryKeyTrait;

    protected $primaryKey = [
        'content_id',
        'locale'
    ];

    protected $fillable = ['value'];

    public $timestamps = false;

    public $incrementing = false;
}
