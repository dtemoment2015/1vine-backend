<?php

namespace App\Models\Storage;

use App\Models\Business\Role;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Navigation extends Model implements TranslatableContract
{

    use HasFactory, Translatable;

    protected $fillable = [
        'code',
        'value',
        'is_active', 
        'activeIcon', 
        'color', 
        'component', 
        'alphaClr', 
        'position', 
        'type', 
        'icon', 
        'platform', 
    ];

    protected $casts = [
        'is_active' => 'bool'
    ];

    public $translatedAttributes = [
        'title',
        'desciption',
    ];

    public function roles()
    {
        return $this->belongsToMany(
            Role::class
        );
    }

    public function blocks()
    {
        return $this->belongsToMany(Block::class)
            ->withPivot('position')
            ->orderBy('block_navigation.position');
    }

    public function sections()
    {
        return $this->hasMany(Section::class);
    }

    public function scopeFilterBlocks($query, $blocks = [])
    {
        return $query->whereHas('blocks', function ($query) use ($blocks) {
            $query->whereIn('code', $blocks);
        });
    }
}
