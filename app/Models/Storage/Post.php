<?php

namespace App\Models\Storage;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Post extends Model  implements TranslatableContract
{

    use HasFactory, Translatable;

    protected $casts = [
        'is_active' => 'boolean'
    ];

    protected $fillable = [
        'is_active',
        'code',
        'position',
    ];


    public $translatedAttributes = [
        'title',
        'annotation',
        'image',
    ];

    public function contents()
    {
        return $this->morphMany(
            Content::class,
            'item'
        )
            ->orderBy('position')
            ->orderBy('id', 'DESC');
    }

}
