<?php

namespace App\Models\Storage;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    use HasFactory;

    public function navigations()
    {
        return $this->belongsToMany(Navigation::class)
            ->withPivot('position')
            ->orderBy('block_navigation.position');
    }
}
