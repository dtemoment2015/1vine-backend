<?php

namespace App\Models\Storage;

use App\Models\Helpers\MultiPrimaryKeyTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NavigationTranslation extends Model
{
    use HasFactory, MultiPrimaryKeyTrait;

    protected $primaryKey = [
        'navigation_id',
        'locale'
    ];

    protected $fillable = ['title'];

    public $timestamps = false;

    public $incrementing = false;
}
