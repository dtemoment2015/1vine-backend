<?php

namespace App\Models\Storage;

use App\Models\User\Transaction;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Status extends Model  implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $casts = [
        'is_active' => 'boolean',
    ];

    const START_STATUS = 'accept';

    public $translatedAttributes = [
        'title',
    ];

    public function item()
    {
        return $this->morphMany(
            Transaction::class,
            'item'
        );
    }
}
