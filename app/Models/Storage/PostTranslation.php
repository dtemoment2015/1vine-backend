<?php

namespace App\Models\Storage;

use App\Library\ImageUpload;
use App\Models\Helpers\MultiPrimaryKeyTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostTranslation extends Model
{
    use HasFactory, MultiPrimaryKeyTrait;

    protected $primaryKey = [
        'post_id',
        'locale'
    ];

    protected $fillable = ['title', 'annotation', 'image'];

    public $timestamps = false;

    public $incrementing = false;

    public function getImageAttribute()
    {
     
        if (optional($this->attributes)['image']) {
            return url('storage/posts/x400/' . $this->attributes['image']);
        }

        return null;
    }

}
