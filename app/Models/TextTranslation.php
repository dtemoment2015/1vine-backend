<?php

namespace App\Models;

use App\Models\Helpers\MultiPrimaryKeyTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TextTranslation extends Model
{

    use HasFactory, MultiPrimaryKeyTrait;

    protected $primaryKey = [
        'text_id',
        'locale'
    ];

    protected $fillable = ['title'];

    public $timestamps = false;

    public $incrementing = false;
    
}
