<?php

namespace App\Models\Business;

use App\Models\Storage\Navigation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Role extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    public $translatedAttributes = [
        'title'
    ];

    protected $fillable = ['code'];

    public function navigations()
    {
        return $this->belongsToMany(
            Navigation::class
        )
        ->orderBy('position');
    }
}
