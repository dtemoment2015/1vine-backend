<?php

namespace App\Models\Business;

use App\Models\Cloud\Location;
use App\Models\User;
use App\Models\User\Dealer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Nicolaslopezj\Searchable\SearchableTrait;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Shop extends Model  implements TranslatableContract, HasMedia
{
    use HasFactory, Translatable, InteractsWithMedia, SearchableTrait;

    protected $searchable = [
        'columns' => [
            'shop_translations.title' => 10,
            'shops.name' => 11,
            'dealers.inn' => 12,
            'dealers.llc_name' => 10,
        ],
        'joins' => [
            'shop_translations' =>
            [
                'shops.id',
                'shop_translations.shop_id'
            ],
            'dealers' =>
            [
                'shops.dealer_id',
                'dealers.id'
            ],
        ],
    ];

    protected $with = [
        // 'business',
        'location',
    ];

    public $translatedAttributes = [
        'title',
        'description',
        'address'
    ];

    protected $fillable = [
        'location_id',
        'business_id',
        'dealer_id',
        'name',
        'lat',
        'lon',
    ];

    public function dealer()
    {
        return $this->belongsTo(
            Dealer::class
        );
    }

    public function users()
    {
        return $this->belongsToMany(
            User::class
        );
    }

    public function business()
    {
        return $this->belongsTo(
            Business::class
        );
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function registerMediaConversions(
        Media $media = null
    ): void {
        $this->addMediaConversion('small')
            ->width(140)
            ->nonQueued();
    }
}
