<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Helpers\MultiPrimaryKeyTrait;

class ShopTranslation extends Model
{

    use HasFactory, MultiPrimaryKeyTrait;

    protected $primaryKey = [
        'shop_id',
        'locale'
    ];

    protected $fillable = [
        'title',
        'address',
        'description'
    ];

    public $timestamps = false;

    public $incrementing = false;
}
