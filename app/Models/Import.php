<?php

namespace App\Models;

use App\Traits\ColumnFillable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Import extends Model   implements HasMedia
{
    use HasFactory, ColumnFillable, InteractsWithMedia;

    public function registerMediaCollections(Media $media = null): void
    {
        $this
            ->addMediaCollection('import_file')
            ->singleFile();
    }
}
