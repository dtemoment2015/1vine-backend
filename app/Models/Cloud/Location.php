<?php

namespace App\Models\Cloud;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Location extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    public $translatedAttributes = [
        'title'
    ];

    public function parent()
    {
        return $this->hasOne(
            Location::class,
            'id',
            'parent_id'
        );
    }

    public function childs()
    {
        return $this->hasMany(
            Location::class,
            'parent_id',
            'id'
        );
    }
}
