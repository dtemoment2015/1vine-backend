<?php

namespace App\Models\User;

use App\Models\Business\Role;
use App\Models\Business\Shop;
use App\Models\User;
use App\Models\User\Dealer\DealerTemp;
use App\Models\Order\Device;
use App\Models\Order\Order;
use App\Models\Warehouse\Option;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;
use Nicolaslopezj\Searchable\SearchableTrait;

class Dealer extends Authenticatable
{

    use HasApiTokens, HasFactory, Notifiable, SearchableTrait;

    protected $searchable = [
        'columns' => [
            'dealers.phone' => 12,
            'dealers.inn' => 10,
            'dealers.llc_name' => 10,
        ],
    ];

    protected $with = ['shops', 'role'];
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */


    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'password',
        'email',
        'code',
        'is_active',
        'role_id',
        'inn',
        'llc_name',
        'accountant_full_name',
        'accountant_phone',
        'organizationa_address',
        'checking_account',
        'mfo',
        'oked',
        'trademark',
    ];

    protected $appends = [
        'channel',
        'full_name'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'email_verified_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'is_active' => 'boolean',
        'email' => 'string',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function getChannelAttribute()
    {
        return md5(md5(optional($this)->id));
    }

    public function shops()
    {
        return $this->hasMany(Shop::class);
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function dealer_temp()
    {
        return $this->hasOne(DealerTemp::class);
    }

    public function setPasswordAttribute($val)
    {
        $this->attributes['password'] = bcrypt($val);
    }

    public function cart()
    {
        return $this->belongsToMany(Option::class)->withPivot('count');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function devices()
    {
        return $this->hasMany(Device::class);
    }

    public function transactions()
    {
        return $this->morphMany(
            Transaction::class,
            'activator'
        );
    }

    public function role()
    {
        return $this->belongsTo(
            Role::class
        );
    }

    public function scopeBonus($query)
    {
        return $query->withCount(['transactions as bonus' => function ($query) {
            $query->select(DB::raw('SUM(value)'));
        }]);
    }
}
