<?php

namespace App\Models\User;

use App\Models\Business\Role;
use App\Models\Business\Shop;
use App\Models\User;
use App\Models\User\Dealer\DealerTemp;
use App\Models\Order\Device;
use App\Models\Order\Order;
use App\Models\Warehouse\Option;
use App\Traits\ColumnFillable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;
use Nicolaslopezj\Searchable\SearchableTrait;

class Client extends Authenticatable
{

    use HasApiTokens, HasFactory, Notifiable, SearchableTrait, ColumnFillable;

    protected $searchable = [
        'columns' => [
            'dealers.id' => 3,
            'dealers.phone' => 9,
            'dealers.first_name' => 4,
            'dealers.last_name' => 4,
        ],
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */

    protected $appends = [
        'channel',
        'full_name'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'email_verified_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'is_active' => 'boolean',
        'email' => 'string',
    ];



    public function getChannelAttribute()
    {
        return md5(md5(optional($this)->id));
    }


    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }


    public function setPasswordAttribute($val)
    {
        $this->attributes['password'] = bcrypt($val);
    }

    public function transactions()
    {
        return $this->morphMany(
            Transaction::class,
            'activator'
        );
    }

    public function scopeBonus($query)
    {
        return $query->withCount(['transactions as bonus' => function ($query) {
            $query->select(DB::raw('SUM(value)'));
        }]);
    }
}
