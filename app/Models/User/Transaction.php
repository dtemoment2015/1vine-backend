<?php

namespace App\Models\User;

use App\Models\Order\Order;
use App\Models\Storage\Status;
use App\Models\User;
use App\Models\Warehouse\Serial;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\Relation;

Relation::enforceMorphMap([
    'dealer' => Dealer::class,
    'client' => Client::class,
    'serial' => Serial::class,
    'status' => Status::class,
    'order' => Order::class,
]);

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'value',
        'barcode',
        'secret',
        'is_cancel',
        'is_confirm',
    ];

    protected $casts  = [
        'is_confirm' => 'boolean',
        'is_cancel' => 'boolean',
        'value' => 'double',
    ];

    public function activator()
    {
        return $this->morphTo();
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function item()
    {
        return $this->morphTo();
    }

    public function extra()
    {
        return $this->morphTo();
    }
}
