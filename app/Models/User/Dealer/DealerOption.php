<?php

namespace App\Models\User\Dealer;

use App\Models\User\Dealer;
use App\Models\Warehouse\Option;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Relations\Pivot;

class DealerOption extends Pivot
{
    use HasFactory;

    public function option()
    {
        return $this->belongsTo(
            Option::class
        );
    }

    public function dealer()
    {
        return $this->belongsTo(
            Dealer::class
        );
    }
}
