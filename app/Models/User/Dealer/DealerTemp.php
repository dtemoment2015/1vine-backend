<?php

namespace App\Models\User\Dealer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DealerTemp extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'address'
    ];
}
