<?php

namespace App\Models;

use App\Models\Business\Shop;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Point extends Model  implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $casts = [
        'lat' => 'float',
        'lon' => 'float',
    ];

    // protected $with = ['media'];

    protected $fillable = [
        'lat',
        'lon',
        'shop_id',
        'device',
    ];

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function registerMediaConversions(
        Media $media = null
    ): void {
        $this->addMediaConversion('small')
            ->width(100)
            ->nonQueued();
    }
}
