<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Logger extends Model
{
    use HasFactory;

    protected $fillable = [
        'uri',
        'method',
        'request_body',
        'response',
        'header',
        'status'
    ];

    protected   $casts = [
        'request_body' => 'array',
        'response' => 'array',
        'header' => 'array',
        'uri' => 'string',
        'status' => 'integer',
    ];

    protected $appends = ['user_agent', 'content_type', 'localization'];

    protected $hidden = ['header'];


    public function getRequestBodyAttribute(){
        return  json_decode(json_decode( $this->attributes['request_body']));
    }

    public function getUserAgentAttribute()
    {
        return optional(optional(optional($this)->header)['user-agent'])[0];
    }
    public function getLocalizationAttribute()
    {
        return optional(optional(optional($this)->header)['localization'])[0];
    }
    public function getContentTypeAttribute()
    {
        return optional(optional(optional($this)->header)['content-type'])[0];
    }
}
