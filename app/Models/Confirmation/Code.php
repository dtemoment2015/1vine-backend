<?php

namespace App\Models\Confirmation;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    const MINUTE_DESTROY = 2;

    use HasFactory;

    protected $fillable = [
        'phone',
        'code'
    ];

    protected $appends = ['before_at'];

    protected $hidden = ['code', 'id', 'updated_at'];

    public function getBeforeAtAttribute()
    {
        return $this->created_at->addMinutes(self::MINUTE_DESTROY);
    }
}
