<?php

namespace App\Models\Confirmation;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AllowedPhone extends Model
{
    use HasFactory;

    public $timestamps = false;

    
    protected $fillable = [
        'phone',
        'hash'
    ];
}
