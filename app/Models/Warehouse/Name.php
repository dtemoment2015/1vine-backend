<?php

namespace App\Models\Warehouse;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

use Nicolaslopezj\Searchable\SearchableTrait;

class Name extends Model  implements TranslatableContract
{
    use HasFactory, Translatable, SearchableTrait;


    protected $searchable = [
        'columns' => [
            'names.code' => 10,
            'name_translations.title' => 8,
        ],
        'joins' => [
            'name_translations' =>
            [
                'names.id',
                'name_translations.name_id'
            ],
        ],
    ];

    protected $fillable = [
        'code',
    ];
    
    public $translatedAttributes = [
        'title'
    ];

    public function features()
    {
        return $this->hasMany(Feature::class);
    }
}
