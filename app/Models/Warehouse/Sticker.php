<?php

namespace App\Models\Warehouse;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sticker extends Model
{
    use HasFactory;

    protected $fillable = [
        'barcode',
        'secret_dealer',
        'secret_buyer',
        'is_active',
        'serial_id',
        'group',
    ];

    protected $casts  = [
        'is_active' => 'boolean',
    ];

    public function serial()
    {
        return $this->belongsTo(Serial::class);
    }

    public function setBarcodeAttribute($val)
    {
        $this->attributes['secret_dealer'] = 'd' . substr(md5($val), 11, 10);
        $this->attributes['secret_buyer'] =  'p' . substr(md5($val), 0, 10);
        $this->attributes['barcode'] = $val;

        return $val;
    }
}
