<?php

namespace App\Models\Warehouse;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class Product extends Model   implements TranslatableContract
{

    use HasFactory, Translatable, SearchableTrait, SoftDeletes;

    public $translatedAttributes = [
        'title'
    ];

    protected $searchable = [
        'columns' => [
            'products.vendor_code' => 7,
            'brands.name' => 12,
            'name_translations.title' => 9,
            'product_translations.title' => 9,
        ],
        'joins' => [
            'brands' => [
                'products.brand_id',
                'brands.id'
            ],
            'name_translations' =>
            [
                'products.name_id',
                'name_translations.name_id'
            ],
            'product_translations' =>
            [
                'products.id',
                'product_translations.product_id'
            ],
        ],
    ];

    protected $fillable = [
        'code',
        'vendor_code',
        'is_active',
        'name_id',
        'brand_id',
        'position',
    ];

    protected $with = [
        'name',
        'brand'
    ];

    protected $casts = [
        'is_active' => 'boolean'
    ];

    public function options()
    {
        return $this->hasMany(Option::class);
    }

    public function name()
    {
        return $this->belongsTo(Name::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function categories()
    {
        return $this->belongsToMany(
            Category::class
        );
    }

    public function scopeStock($query)
    {
        return $query
            ->whereHas('options', function ($query) {
                $query->stock();
            });
    }
}
