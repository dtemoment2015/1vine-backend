<?php

namespace App\Models\Warehouse;

use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Relations\Pivot;

class CategoryProduct extends Pivot
{

    use HasFactory;

    public function products()
    {
        return $this->hasMany(
            Product::class
        );
    }

    public function categoris()
    {
        return $this->hasMany(
            Category::class
        );
    }
}
