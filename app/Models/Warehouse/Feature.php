<?php

namespace App\Models\Warehouse;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

use Nicolaslopezj\Searchable\SearchableTrait;

class Feature extends Model  implements TranslatableContract
{
    use HasFactory, Translatable, SearchableTrait;



    protected $searchable = [
        'columns' => [
            'features.code' => 10,
            'feature_translations.title' => 8,
        ],
        'joins' => [
            'feature_translations' =>
            [
                'features.id',
                'feature_translations.feature_id'
            ],
        ],
    ];

    public $translatedAttributes = [
        'title'
    ];

    protected $casts = [
        'is_active' => 'boolean',
        'is_filter' => 'boolean',
        'is_main' => 'boolean',
    ];

    protected $fillable = [
        'code',
        'name_id',
        'parent_id',
        'is_filter',
        'is_main',
        'position',
    ];

    public function values()
    {
        return $this->belongsToMany(
            Value::class,
            'feature_option',
            'feature_id',
            'value_id'
        )
            ->withPivot('option_id')
            ->groupBy('value_id')
            ->orderByTranslation('title');
    }

    public function name()
    {
        return $this->belongsTo(Name::class);
    }

    public function parent()
    {
        return $this->hasOne(
            self::class,
            'id',
            'parent_id'
        );
    }

    public function childs()
    {
        return $this->hasMany(
            self::class,
            'parent_id',
            'id'
        );
    }
}
