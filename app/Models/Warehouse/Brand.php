<?php

namespace App\Models\Warehouse;

use App\Library\ImageUpload;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Brand extends Model
{
    use HasFactory, SearchableTrait;

    protected $searchable = [
        'columns' => [
            'brands.name' => 12,
            'brands.code' => 12,
        ],
    ];

    protected $casts = [
        'is_active' => 'boolean'
    ];

    protected $fillable = [
        'name',
        'code',
        'is_active',
        'position',
        'image'
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function setImageAttribute($val)
    {
        if ($val && !filter_var($val, FILTER_VALIDATE_URL)) {
            $this->attributes['image'] = ImageUpload::load(
                $val,
                '',
                'brands',
                [220]
            );
        }
    }
}
