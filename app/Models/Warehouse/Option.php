<?php

namespace App\Models\Warehouse;

use App\Models\Storage\Content;
use App\Models\User\Dealer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Nicolaslopezj\Searchable\SearchableTrait;

class Option extends Model
{
    use HasFactory, SearchableTrait, SoftDeletes;

    protected $perPage = 60;

    protected $fillable = [
        'product_id',
        'sku',
        'price',
        'price_old',
        'price_retail',
        'reward',
        'quantity',
        'name',
        'is_active'
    ];

    protected $searchable = [
        'columns' => [
            'options.name' => 10,
            'options.sku' => 3,
            'products.vendor_code' => 7,
            'brands.name' => 12,
            'name_translations.title' => 9,
            'product_translations.title' => 9,
        ],
        'joins' => [
            'products' => [
                'options.product_id',
                'products.id'
            ],
            'brands' => [
                'products.brand_id',
                'brands.id'
            ],
            'name_translations' =>
            [
                'products.name_id',
                'name_translations.name_id'
            ],
            'product_translations' =>
            [
                'products.id',
                'product_translations.product_id'
            ],
        ],
    ];

    protected $with = [
        'product',
        'images'
    ];

    protected $casts = [
        'is_active' => 'boolean'
    ];

    public function values()
    {
        return $this->belongsToMany(
            Value::class
        )
            ->withPivot('feature_id');
    }

    public function features()
    {
        return $this->belongsToMany(
            Feature::class
        )
            ->withPivot('value_id');
    }

    public function brand()
    {
        return $this->hasOneThrough(Brand::class, Product::class);
    }

    public function product()
    {
        return $this->belongsTo(
            Product::class
        );
    }

    public function serials()
    {
        return $this->hasMany(
            Serial::class
        );
    }

    //УДАЛИТЬ И ПЕРЕВЕСТИ НА MEDIA
    public function images()
    {
        return $this->hasMany(
            Image::class
        )
            ->orderBy('position');
    }

    public function contents()
    {
        return $this->morphMany(
            Content::class,
            'item'
        )
            ->orderBy('position')
            ->orderBy('id', 'DESC');
    }

    public function scopeStock($query)
    {
        return $query->whereIsActive(true)
            ->where('quantity', '>', 0)
            // ->whereHas('images');
        ;
    }

    public function dealer()
    {
        return $this->belongsToMany(Dealer::class)
            ->withPivot('count');
    }

    public function modifications()
    {
        return $this->hasMany(
            self::class,
            'product_id',
            'product_id'
        );
    }

    public function categories()
    {
        return $this->belongsToMany(
            Category::class
        );
    }

    public function scopeFilter($query)
    {
        return
            $query
            ->when(request('search'), function ($query) {
                $query->search(request('search'))
                    ->orderBy('relevance', 'DESC');
            })
            ->when(request('category_id'), function ($query) {
                $query->whereHas('product.categories', function ($query) {
                    $query->whereId(request('category_id'));
                });
            })
            ->when(request('brand_ids'), function ($query) {
                $query->whereHas('product.brand', function ($query) {
                    $query->whereIn('id', (array) request('brand_ids'));
                });
            })
            ->when(request('price_min'), function ($query) {
                $query->where('price', '>=', (float) request('price_min'));
            })
            ->when(request('price_max'), function ($query) {
                $query->where('price', '<=',  (float)  request('price_max'));
            })
            ->when(request('categories_id'), function ($query) {
                $query->whereHas('product.categories', function ($query) {
                    $query->whereIn('id', explode('-', request('categories_id')));
                });
            })
            ->when(request('brands_id'), function ($query) {
                $query->whereHas('product.brand', function ($query) {
                    $query->whereIn('id', explode('-', request('brands_id')));
                });
            })
            ->when(request('names_id'), function ($query) {
                $query->whereHas('product.name', function ($query) {
                    $query->whereIn('id', explode('-', request('names_id')));
                });
            })
            //features
            ->when(request('feature_ids'), function ($query) {
                foreach (collect(request('feature_ids', [])) as $k => $params) {
                    $query->whereHas('values', function ($query) use ($params, $k) {
                        $query->where('feature_id',  (int) $k)
                            ->whereIn('value_id', (array) $params);
                    });
                }
            })
            ->when(request('order'), function ($query) {
                $order = request('order');
                $query->when($order == 'price', function ($query) {
                    $query->orderBy('price', 'DESC');
                })->when($order == '-price', function ($query) {
                    $query->orderBy('price', 'ASC');
                })
                    ->when($order == 'new', function ($query) {
                        $query->orderBy('created_at', 'DESC');
                    })->when($order == '-new', function ($query) {
                        $query->orderBy('created_at', 'ASC');
                    })
                    ->when($order == 'quantity', function ($query) {
                        $query->orderBy('quantity', 'DESC');
                    })->when($order == '-quantity', function ($query) {
                        $query->orderBy('quantity', 'ASC');
                    })
                    ->when($order == 'sku', function ($query) {
                        $query->orderBy('sku', 'DESC');
                    })->when($order == '-sku', function ($query) {
                        $query->orderBy('sku', 'ASC');
                    })
                    ->when($order == 'reward', function ($query) {
                        $query->orderBy('reward', 'DESC');
                    })->when($order == '-reward', function ($query) {
                        $query->orderBy('reward', 'ASC');
                    })
                    ->when($order == 'id', function ($query) {
                        $query->orderBy('id', 'DESC');
                    })->when($order == '-id', function ($query) {
                        $query->orderBy('id', 'ASC');
                    });
            })
            ->orderBy('position')
            ->orderBy('id', 'DESC')
            ->groupBy('id');
    }


    public function scopeDealer($query)
    {
        $user = Auth::guard('dealer')->user();

        return $query->when($user, function ($query) use ($user) {
            $query->with(['dealer' => function ($query) use ($user) {
                $query->whereId($user->id);
            }]);
        });
    }

    public function scopeFeature($query)
    {
        return $query->with(['values' => function ($query) {
            $query
                ->whereHas('key.feature.parent')
                ->with(['key.feature.parent']);
        }]);
    }
}
