<?php

namespace App\Models\Warehouse;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

use Nicolaslopezj\Searchable\SearchableTrait;

class Category extends Model implements TranslatableContract
{
    use HasFactory, Translatable, SearchableTrait;

    protected $searchable = [
        'columns' => [
            'category_translations.title' => 7,
            'categories.code' => 12,
        ],
        'joins' => [
            'category_translations' =>
            [
                'categories.id',
                'category_translations.category_id'
            ],
        ],
    ];

    protected $casts = [
        'is_active' => 'boolean',
        'in_catalog' => 'boolean',
    ];

    protected $fillable = ['position', 'parent_id', 'is_active'];

    public $translatedAttributes = [
        'title'
    ];

    public function parent()
    {
        return $this->hasOne(
            self::class,
            'id',
            'parent_id'
        );
    }

    public function childs()
    {
        return $this->hasMany(
            self::class,
            'parent_id',
            'id'
        )
            ->whereIsActive(true)
            ->with('childs');
    }

    public function products()
    {
        return $this->belongsToMany(
            Product::class
        );
    }

    public function options()
    {
        return $this->belongsToMany(
            Option::class
        );
    }
}
