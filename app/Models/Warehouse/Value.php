<?php

namespace App\Models\Warehouse;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Nicolaslopezj\Searchable\SearchableTrait;

class Value extends Model  implements TranslatableContract
{
    use HasFactory, Translatable, SearchableTrait;

    protected $searchable = [
        'columns' => [
            'values.code' => 10,
            'value_translations.title' => 8,
        ],
        'joins' => [
            'value_translations' =>
            [
                'values.id',
                'value_translations.value_id'
            ],
        ],
    ];

    public $translatedAttributes = [
        'title'
    ];

    protected $fillable = [
        'code',
        'position',
    ];

    public function key()
    {
        return $this->hasOne(
            OptionValue::class
        );
    }

    public function option()
    {
        return $this->belongsToMany(
            Option::class
        );
    }

    public function features()
    {
        return $this->belongsToMany(
            Feature::class,
            'feature_option',
            'value_id',
            'feature_id'
        );
    }
}
