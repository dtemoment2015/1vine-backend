<?php

namespace App\Models\Warehouse;

use App\Library\ImageUpload;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{

    //УДАЛИТЬ И ПЕРЕВЕСТИ НА MEDIA
    use HasFactory;

    public $timestamps = false;

    protected $casts = [
        'is_active' => 'boolean'
    ];

    protected $fillable = [
        'file',
        'position'
    ];

    public function setFileAttribute($val)
    {
        if ($val && !filter_var($val, FILTER_VALIDATE_URL)) {
            $this->attributes['file'] = ImageUpload::load(
                $val,
                '',
                'options',
                [600, 200]
            );
        }
    }
}
