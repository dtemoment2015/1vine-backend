<?php

namespace App\Models\Warehouse;

use App\Models\Helpers\MultiPrimaryKeyTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ColorTranslation extends Model
{
    use HasFactory, MultiPrimaryKeyTrait;

    protected $primaryKey = [
        'color_id',
        'locale'
    ];
    
    protected $fillable = ['title'];

    public $timestamps = false;

    public $incrementing = false;
}
