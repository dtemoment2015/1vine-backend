<?php

namespace App\Models\Warehouse;

use App\Models\Helpers\MultiPrimaryKeyTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ValueTranslation extends Model
{
    use HasFactory, MultiPrimaryKeyTrait;

    protected $primaryKey = [
        'name_id',
        'locale'
    ];

    protected $fillable = ['title'];

    public $timestamps = false;

    public $incrementing = false;
}
