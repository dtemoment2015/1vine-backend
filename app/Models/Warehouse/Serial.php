<?php

namespace App\Models\Warehouse;

use App\Models\User\Transaction;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Serial extends Model
{
    use HasFactory;

    protected $fillable = [
        'barcode',
        'name',
        'is_active',
        'is_paid',
        'is_wrote',
    ];

    protected $casts  = [
        'is_active' => 'boolean',
        'is_paid' => 'boolean',
        'is_wrote' => 'boolean',
    ];

    public function option()
    {
        return $this->belongsTo(Option::class);
    }

    public function sticker()
    {
        return $this->hasOne(Sticker::class);
    }

    public function item()
    {
        return $this->morphMany(
            Transaction::class,
            'item'
        );
    }
}
