<?php

namespace App\Models\Warehouse;

use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Relations\Pivot;

class OptionValue extends Pivot
{

    use HasFactory;

    public function option()
    {
        return $this->belongsTo(
            Option::class,
        );
    }

    public function value()
    {
        return $this->belongsTo(
            Value::class,
        );
    }

    public function feature()
    {
        return $this->belongsTo(
            Feature::class,
        );
    }
}
