<?php

namespace App\Models\Helpers;

use Illuminate\Database\Eloquent\Builder;

trait Scroll
{
   public function scopeScroll($query, $count = 10)
   {
      $query
         ->offset((int)request('offset', 0))
         ->limit((int)request('limit', $count));
   }
}
