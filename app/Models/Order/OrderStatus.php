<?php

namespace App\Models\Order;

use App\Models\Storage\Status;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'status_id'
    ];

    protected $with = [
        'status'
    ];

    public function status()
    {
        return $this->belongsTo(
            Status::class
        )->whereType('order');
    }

    public function user()
    {
        return $this->belongsTo(
            User::class
        );
    }


}
