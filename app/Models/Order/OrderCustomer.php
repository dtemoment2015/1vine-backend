<?php

namespace App\Models\Order;

use App\Models\Business\Shop;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderCustomer extends Model
{
    use HasFactory;

    protected $primaryKey = 'order_id';

    protected $fillable = [
        'name',
        'phone',
        'address',
        'location_id',
        'shop_id',
    ];

    public $timestamps = false;

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }
}
