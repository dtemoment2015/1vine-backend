<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderPurchase extends Model
{
    use HasFactory;

    protected $fillable = [
        'option_id',
        'display_name',
        'sku',
        'image',
        'price',
        'price_old',
        'quantity',
        'reward',
    ];

    protected $appends = ['price_total'];

    public function getPriceTotalAttribute()
    {
        return (int) optional($this->attributes)['price'] * (int) optional($this->attributes)['quantity'];
    }

    public function order()
    {
        return $this->belongsTo(
            Order::class
        );
    }
}
