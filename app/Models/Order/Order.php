<?php

namespace App\Models\Order;

use App\Models\User;
use App\Models\User\Dealer;
use App\Models\User\Transaction;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Nicolaslopezj\Searchable\SearchableTrait;

class Order extends Model
{

    use HasFactory, SearchableTrait;

    protected $searchable = [
        'columns' => [
            'orders.id' => 10,
            'order_customers.name' => 3,
            'order_customers.phone' => 3,
        ],
        'joins' => [
            'order_customers' => [
                'orders.id',
                'order_customers.order_id'
            ],
        ],
    ];

    protected $fillable = [
        'code',
        'is_paid',
        'paid_at',
        'dealer_id',
    ];

    protected $casts = [
        'is_paid' => 'boolean'
    ];

    protected $with = [
        'customer',
        'status',
    ];

    public function transaction()
    {
        return $this->morphMany(
            Transaction::class,
            'item'
        );
    }


    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function statuses()
    {
        return $this->hasMany(OrderStatus::class);
    }

    //for supervisor
    public function attach_users()
    {
        return $this->belongsToMany(User::class);
    }


    public function customer()
    {
        return $this->hasOne(OrderCustomer::class);
    }

    public function dealer()
    {
        return $this->belongsTo(Dealer::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function purchases()
    {
        return $this->hasMany(OrderPurchase::class);
    }

    public function status()
    {
        return $this->belongsTo(OrderStatus::class, 'order_status_id');
    }


    public function scopeDateFilter($query)
    {
        try {
            if (request('from_at') && $dateStart = Carbon::parse(request('from_at'))) {
                $query->when($dateStart, function ($q) use ($dateStart) {
                    $q->whereDate('created_at', '>=', $dateStart->startOfDay());
                });
            }
        } catch (Exception $e) {
        }
        try {
            if (request('till_at') && $dateEnd = Carbon::parse(request('till_at'))) {
                $query->when($dateEnd, function ($q) use ($dateEnd) {
                    $q->whereDate('created_at', '<=', $dateEnd->endOfDay());
                });
            }
        } catch (Exception $e) {
        }
    }

    public function scopePrice($query)
    {
        return $query->withCount(['purchases as price' => function ($query) {
            $query->select(DB::raw('SUM(order_purchases.price * order_purchases.quantity)'));
        }]);
    }

    public function scopePayment($query)
    {
        return $query->withSum('payments as payment','amount');
    }

    public function scopeReward($query)
    {
        return $query->withCount(['purchases as reward' => function ($query) {
            $query->select(DB::raw('SUM(order_purchases.reward * order_purchases.quantity)'));
        }]);
    }
}
