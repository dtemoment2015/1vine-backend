<?php

namespace App\Exceptions;

use Exception;

class MyOrderException extends Exception
{
    public $message = '';
    public $errors = [];

    public function __construct($message, $errors)
    {
        parent::__construct();

        $this->message = $message;
        $this->errors = $errors;
    }

    public function render($request)
    {
        return response()->json(["errors" => $this->errors, "message" => $this->message], 422);
    }
}
