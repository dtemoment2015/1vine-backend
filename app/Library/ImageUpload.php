<?php

namespace App\Library;

use Exception;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ImageUpload
{
   /**
    * @param null $file
    * @param string $ID
    * @param array $resizes
    * @param string $folder
    * @return null|string
    */
   public static function load(
      $file = null,
      $ID = '',
      $folder = 'images',
      $resizes = [],
      $staticName = null
   ) {
      if ($file) {
         try {

            //принудительное имя

            $img = Image::make($file);
            $mime = $img->mime();
            $extension = explode('/', $mime)[1];
            if ($staticName) {
               $fileName = $staticName . '.' . $extension;
            } else {
               $fileName = $ID . '_' . time() . setCode() . '.' . $extension;
            }
            Storage::disk('local')->put('public/' . $folder . '/' . $ID . '/' . $fileName, (string) $img->encode());
            if (count($resizes)) {
               foreach ($resizes as $resize) {
                  if (is_numeric($resize) && $resize > 0) {
                     $imgPreview = Image::make((string) $img->encode())
                        ->resize(
                           $resize,
                           null,
                           function ($constraint) {
                              $constraint->aspectRatio();
                           }
                        );
                     Storage::disk('local')
                        ->put(
                           '/public/' . $folder . '/' . $ID . '/x' . $resize . '/' . $fileName,
                           (string) $imgPreview->encode()
                        );
                     usleep(300);
                  }
               }
            }

            return $fileName;
         } catch (Exception $e) {

            return null;
         }
      }

      return null;
   }
}
