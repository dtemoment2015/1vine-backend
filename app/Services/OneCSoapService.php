<?php


namespace App\Services;

use App\Exceptions\MyOrderException;
use Illuminate\Support\Facades\Log;

class OneCSoapService
{
    public function send(
        $method = 'Orders',
        $arg = []
    ) {
        try {
            $opts = [
                'http' => [
                    'user_agent' => 'PHPSoapClient'
                ]
            ];
            $context = stream_context_create(
                $opts
            );
            $soapClientOptions = [
                'login' => env('SOAP_LOGIN'),
                'password' => env('SOAP_PASSWORD'),
                'stream_context' => $context,
                'cache_wsdl' => WSDL_CACHE_NONE,
            ];
            $client = new \SoapClient(
                env('SOAP_END_POINT'),
                $soapClientOptions
            );

            $data = ['Order' => json_encode(['data' => $arg])];

            $result = $client->$method(
                $data
            );

            Log::info($result->return);

            return $result->return;
        } catch (\Exception $e) {

            Log::info($e->getMessage());
        }
    }
}
