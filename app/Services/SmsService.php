<?php

namespace App\Services;

use GuzzleHttp\Client as Client;
use GuzzleHttp\RequestOptions as RequestOptions;
use GuzzleHttp\Exception\ClientException;

class SmsService
{
    public static function send($client = null, $message = '')
    {
        $clientHttp = new Client();

        try {
            $phone = str_replace('+', '',  $client->phone);
            return   $clientHttp->post(
                env('SMS_URL'),
                [
                    RequestOptions::JSON => [
                        'login' =>  env('SMS_LOGIN'),
                        'key' =>   env('SMS_KEY'),
                        'sender' => env('SMS_SENDER'),
                        'phone' => $phone,
                        'text' => $message,
                        "weight" => "10"
                    ],
                    'headers' => [
                        'Content-Type' => 'application/json',
                    ]
                ]
            );
            return true;
        } catch (ClientException $e) {
            return false;
        }
    }
}
